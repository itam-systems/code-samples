'use strict';
/**
 * @ngdoc overview
 * @name firstOfficeApp
 * @description
 * # firstOfficeApp
 *
 * Main module of the application.
 */
var app = angular
    .module('firstOfficeApp', [
        'oc.lazyLoad',
        'ui.router',
        'ui.bootstrap',
        'angular-loading-bar',
        'ngFlash'
    ]);


app.config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider', '$httpProvider', 'FlashProvider', function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider, $httpProvider, FlashProvider) {
    //FlashProvider.setTemplate("<div class='alert' style='position: absolute;'>{{ flash.text }}</div>");
    $ocLazyLoadProvider.config({
        debug:true,
        events:true,
        serie: true
    });

    $urlRouterProvider.otherwise('/404');

    $stateProvider
        .state('login',{
            templateUrl: 'views/pages/login.html',
            controller: 'LoginController',
            url:'/login',
            resolve: {
                loadMyFiles:function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name:'firstOfficeApp',
                        files:[
                            'js/angular/utils/BaseService.js',
                            'js/angular/controllers/LoginController.js',
                            'js/angular/services/UserService.js'
                        ]
                    })
                }
            }
        })
        .state('dashboard', {
            url:'',
            controller: 'DashboardController',
            templateUrl: 'views/dashboard/main.html',
            resolve: {
                loadMyFiles:function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name:'firstOfficeApp',
                        files:[
                            'js/angular/utils/BaseService.js',
                            'js/angular/utils/Validators.js',
                            'js/angular/utils/FileUtils.js',
                            'js/angular/controllers/DashboardController.js',
                            'js/angular/services/DashboardService.js',
                            'js/angular/services/UserService.js',
                            'js/angular/services/CompanyService.js',,

                        ]
                    })
                },
                loadMyDirectives:function($ocLazyLoad){
                    return $ocLazyLoad.load(
                        {
                            name:'firstOfficeApp',
                            files:[
                                'js/angular/directives/header/header.js',
                                'js/angular/directives/header/header-notification/header-notification.js',
                                'js/angular/directives/sidebar/sidebar.js',
                                'js/angular/directives/sidebar/sidebar-search/sidebar-search.js'
                            ]
                        }),
                        $ocLazyLoad.load(
                            {
                                name:'toggle-switch',
                                files:["js/angular/lib/angular-toggle-switch/angular-toggle-switch.min.js",
                                    "js/angular/lib/angular-toggle-switch/angular-toggle-switch.css"
                                ]
                            }),
                        $ocLazyLoad.load(
                            {
                                name:'ngAnimate',
                                files:['js/angular/lib/angular-animate/angular-animate.js']
                            })
                    $ocLazyLoad.load(
                        {
                            name:'ngCookies',
                            files:['js/angular/lib/angular-cookies/angular-cookies.js']
                        })
                    $ocLazyLoad.load(
                        {
                            name:'ngResource',
                            files:['js/angular/lib/angular-resource/angular-resource.js']
                        })
                    $ocLazyLoad.load(
                        {
                            name:'ngSanitize',
                            files:['js/angular/lib/angular-sanitize/angular-sanitize.js']
                        })
                    $ocLazyLoad.load(
                        {
                            name:'ngTouch',
                            files:['js/angular/lib/angular-touch/angular-touch.js']
                        })
                }
            }
        })
        .state('dashboard.home',{
            url:'/home',
            controller: 'MainCtrl',
            templateUrl:'views/dashboard/home.html',
            resolve: {
                loadMyFiles:function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'firstOfficeApp',
                        files:[
                            'js/angular/controllers/main.js',
                            'js/angular/directives/timeline/timeline.js',
                            'js/angular/directives/notifications/notifications.js',
                            'js/angular/directives/dashboard/stats/stats.js'
                        ]
                    })
                }
            }
        })
        .state('dashboard.user', {
                url: '/user',
                resolve: {
                    loadMyFiles:function($ocLazyLoad, $injector) {
                        return $ocLazyLoad.load([{
                            name: 'firstOfficeApp',
                            files: [
                                'js/angular/controllers/UserController.js',
                                'js/angular/directives/upload-thumbnail.js'
                            ]
                        },
                            {
                                name: 'xeditable',
                                files: [
                                    'template/xeditable/js/xeditable.js',
                                    'template/xeditable/css/xeditable.css'
                                ]
                            },
                            {
                                name: 'angularFileUpload',
                                files: [
                                    'template/angular-file-upload/angular-file-upload.js'
                                ]
                            }
                        ]).then(function(){
                            var editableOptions = $injector.get('editableOptions');
                            editableOptions.theme = 'bs3';
                        });
                    }
                },
                views: {
                    '': {
                        templateUrl: 'views/dashboard/user.html',
                        controller: 'UserController',
                    },
                    'base@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_base-data.html'
                    },
                    'company@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_company-data.html'
                    },
                    'gusForm@dashboard.user' : {
                        templateUrl: 'views/dashboard/company/_gus-form.html'
                    },
                    'companyForm@dashboard.user': {
                        templateUrl: 'views/dashboard/company/_edit-form.html'
                    },
                    'logoForm@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_logo-form.html'
                    },
                    'ibanForm@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_iban-add.html'
                    },
                    'ibanList@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_iban-list.html'
                    },
                    'invoiceFormatForm@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_invoice-number-format.html'
                    },
                    'userMail@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_user-mail.html'
                    },
                    'userMailAddress@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_user-mail-address.html'
                    },
                    'userMailContact@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_user-mail-contact.html'
                    },
                    'lawBase@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_law-base.html'
                    },
                    'userConfig@dashboard.user': {
                        templateUrl: 'views/dashboard/user/_user-config.html'
                    }
                }
            }
        )
        .state('dashboard.usercreate', {
                url: '/usercreate',
                controller: 'UserController',
                templateUrl: 'views/dashboard/user-create.html',
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load([{
                            name: 'firstOfficeApp',
                            files: [
                                'js/angular/controllers/UserController.js'
                            ]
                        }])
                    }
                }
            }
        )
        .state('dashboard.users', {
                url: '/users',
                controller: 'UsersController',
                templateUrl: 'views/dashboard/users.html',
                serie: true,
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'datatables',
                                files: [
                                    'template/datatables/jquery.datatables.min.js',
                                    'template/datatables/angular-datatables.js',
                                    'template/datatables/css/angular-datatables.css',
                                ]
                            },
                            {
                                name: 'datatables.bootstrap',
                                files: [
                                    'template/datatables/plugins/bootstrap/angular-datatables.bootstrap.min.js',
                                    'template/datatables/plugins/bootstrap/datatables.bootstrap.min.css'
                                ]
                            },
                            {
                                name: 'firstOfficeApp',
                                files: [
                                    'js/angular/controllers/UsersController.js',
                                ]
                            },
                        ]);
                    }
                }
            }
        )
        .state('dashboard.changePassword', {
            templateUrl: 'views/dashboard/change-password.html',
            controller: 'UserController',
            url: '/change-password',
            resolve: {
                loadMyFiles:function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'firstOfficeApp',
                        files: [
                            'js/angular/controllers/UserController.js'
                        ]
                    })
                }
            }
        })
        .state('dashboard.chat',{
            templateUrl:'views/dashboard/chat/chat.html',
            controller: 'MessagesController',
            url:'/chat',
            resolve: {
                loadMyFiles:function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name:'firstOfficeApp',
                            files:[
                                'js/angular/controllers/MessagesController.js',
                                'js/angular/directives/upload-thumbnail.js'
                            ]
                        },
                        {
                            name: 'angularFileUpload',
                            files: [
                                'template/angular-file-upload/angular-file-upload.js'
                            ]
                        }
                    ])
                }
            }
        })
        .state('dashboard.form',{
            templateUrl: 'views/form.html',
            url: '/form'
        })
        .state('dashboard.blank',{
            templateUrl: 'views/pages/blank.html',
            url: '/blank'
        })
        .state('dashboard.chart',{
            templateUrl:'views/chart.html',
            url:'/chart',
            controller:'ChartCtrl',
            resolve: {
                loadMyFile:function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name:'chart.js',
                        files:[
                            'js/angular/lib/angular-chart.js/dist/angular-chart.min.js',
                            'js/angular/lib/angular-chart.js/dist/angular-chart.css'
                        ]
                    }),
                        $ocLazyLoad.load({
                            name:'firstOfficeApp',
                            files:['js/angular/controllers/chartController.js']
                        })
                }
            }
        })
        .state('dashboard.table',{
            templateUrl:'views/table.html',
            url:'/table'
        })
        .state('dashboard.panels-wells',{
            templateUrl:'views/ui-elements/panels-wells.html',
            url:'/panels-wells'
        })
        .state('dashboard.buttons',{
            templateUrl:'views/ui-elements/buttons.html',
            url:'/buttons'
        })
        .state('dashboard.notifications',{
            templateUrl:'views/ui-elements/notifications.html',
            url:'/notifications'
        })
        .state('dashboard.typography',{
            templateUrl:'views/ui-elements/typography.html',
            url:'/typography'
        })
        .state('dashboard.icons',{
            templateUrl:'views/ui-elements/icons.html',
            url:'/icons'
        })
        .state('dashboard.grid',{
            templateUrl:'views/ui-elements/grid.html',
            url:'/grid'
        })
        .state('404',{
            templateUrl:'views/pages/404.html',
            url:'/404',
        });
    $httpProvider.interceptors.push('authInterceptor');
}]);
app.factory('authInterceptor', function ($q, $window, $location) {
    return {
        request: function (config) {
            console.log("Interceptor running");
            if ($window.sessionStorage.access_token) {
                //HttpBearerAuth
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.access_token;
            }
            return config;
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                $location.path('/login').replace();
                //$state.go('login')
            }
            return $q.reject(rejection);
        }
    };
});
app.run(function ($rootScope, $location) {
    $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
        $rootScope.lastState = from;
        $rootScope.lastStateParams = fromParams;

    });
});