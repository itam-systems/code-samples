angular.module('firstOfficeApp')
    .factory('BaseService', function($window, $http, $log, $q, $state, Flash) {
        return {
            makePostRequest: function (url, data) {
                var deferred = $q.defer();
                $http.post(url, data)
                    .success(function (data) {
                        if (data.status === 'success') {
                            if (typeof data.message !== 'undefined' && data.message !== null && data.message !== '') {
                                Flash.create('success', data.message);
                            }
                            deferred.resolve(data.data);
                        }
                        else {
                            Flash.create('danger', data.message);
                            deferred.reject(data.message);
                        }
                    })
                    .error(function (message, code) {
                        deferred.reject(message);
                        if (code === 404) {
                            $state.go('404');
                        }
                    });
                return deferred.promise;
            },
            makeGetRequest: function (url) {
                var deferred = $q.defer();
                $http.get(url)
                    .success(function (data) {
                        if (data.status === 'success') {
                            if (typeof data.message !== 'undefined' && data.message !== null && data.message !== '') {
                                Flash.create('success', data.message);
                            }
                            deferred.resolve(data.data);
                        }
                        else {
                            Flash.create('danger', data.message);
                            deferred.reject(data.message);
                        }
                    })
                    .error(function (message, code) {
                        deferred.reject(message);
                        if (code === 404) {
                            $state.go('404');
                        }
                    });
                return deferred.promise;
            },
            makeStaticFileRequest: function (url){
                var deferred = $q.defer();
                $http.get(url)
                    .success(function (data) {
                        deferred.resolve(data);
                    })
                    .error(function (message, code) {
                        deferred.reject(message);
                        if (code === 404) {
                            $state.go('404');
                        }
                    });
                return deferred.promise;
            },
            makeValidationRequest: function(url, data){
                var deferred = $q.defer();
                $http.post(url, data)
                    .success(function (data) {
                        if (data.status === 'success') {
                            if(data.data.free){

                                deferred.resolve();
                            }
                            else{
                                deferred.resolve(data.message);
                            }
                        }
                        else {
                            Flash.create('danger', data.message);
                            deferred.reject(data.message);
                        }
                    })
                    .error(function (message, code) {
                        deferred.reject(message);
                        if (code === 404) {
                            $state.go('404');
                        }
                    });
                return deferred.promise;
            }
        }
    });