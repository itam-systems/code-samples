angular.module('firstOfficeApp')
    .factory('Validators', function($q) {
        return {
            emailFormat: /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/,
            nipFormat: /^(PL){0,1}[0-9]{10}$/,
            postalFormat: /^dd-ddd$/,
            postals: {
                "GB": "GIR[ ]?0AA|((AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\\d{1,4}",
                "JE": "JE\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}",
                "GG": "GY\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}",
                "IM": "IM\\d[\\dA-Z]?[ ]?\\d[ABD-HJLN-UW-Z]{2}",
                "US": "\\d{5}([ \-]\\d{4})?",
                "CA": "[ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJ-NPRSTV-Z][ ]?\\d[ABCEGHJ-NPRSTV-Z]\\d",
                "DE": "\\d{5}",
                "JP": "\\d{3}-\\d{4}",
                "FR": "\\d{2}[ ]?\\d{3}",
                "AU": "\\d{4}",
                "IT": "\\d{5}",
                "CH": "\\d{4}",
                "AT": "\\d{4}",
                "ES": "\\d{5}",
                "NL": "\\d{4}[ ]?[A-Z]{2}",
                "BE": "\\d{4}",
                "DK": "\\d{4}",
                "SE": "\\d{3}[ ]?\\d{2}",
                "NO": "\\d{4}",
                "BR": "\\d{5}[\-]?\\d{3}",
                "PT": "\\d{4}([\-]\\d{3})?",
                "FI": "\\d{5}",
                "AX": "22\\d{3}",
                "KR": "\\d{3}[\-]\\d{3}",
                "CN": "\\d{6}",
                "TW": "\\d{3}(\\d{2})?",
                "SG": "\\d{6}",
                "DZ": "\\d{5}",
                "AD": "AD\\d{3}",
                "AR": "([A-HJ-NP-Z])?\\d{4}([A-Z]{3})?",
                "AM": "(37)?\\d{4}",
                "AZ": "\\d{4}",
                "BH": "((1[0-2]|[2-9])\\d{2})?",
                "BD": "\\d{4}",
                "BB": "(BB\\d{5})?",
                "BY": "\\d{6}",
                "BM": "[A-Z]{2}[ ]?[A-Z0-9]{2}",
                "BA": "\\d{5}",
                "IO": "BBND 1ZZ",
                "BN": "[A-Z]{2}[ ]?\\d{4}",
                "BG": "\\d{4}",
                "KH": "\\d{5}",
                "CV": "\\d{4}",
                "CL": "\\d{7}",
                "CR": "\\d{4,5}|\\d{3}-\\d{4}",
                "HR": "\\d{5}",
                "CY": "\\d{4}",
                "CZ": "\\d{3}[ ]?\\d{2}",
                "DO": "\\d{5}",
                "EC": "([A-Z]\\d{4}[A-Z]|(?:[A-Z]{2})?\\d{6})?",
                "EG": "\\d{5}",
                "EE": "\\d{5}",
                "FO": "\\d{3}",
                "GE": "\\d{4}",
                "GR": "\\d{3}[ ]?\\d{2}",
                "GL": "39\\d{2}",
                "GT": "\\d{5}",
                "HT": "\\d{4}",
                "HN": "(?:\\d{5})?",
                "HU": "\\d{4}",
                "IS": "\\d{3}",
                "IN": "\\d{6}",
                "ID": "\\d{5}",
                "IL": "\\d{5}",
                "JO": "\\d{5}",
                "KZ": "\\d{6}",
                "KE": "\\d{5}",
                "KW": "\\d{5}",
                "LA": "\\d{5}",
                "LV": "\\d{4}",
                "LB": "(\\d{4}([ ]?\\d{4})?)?",
                "LI": "(948[5-9])|(949[0-7])",
                "LT": "\\d{5}",
                "LU": "\\d{4}",
                "MK": "\\d{4}",
                "MY": "\\d{5}",
                "MV": "\\d{5}",
                "MT": "[A-Z]{3}[ ]?\\d{2,4}",
                "MU": "(\\d{3}[A-Z]{2}\\d{3})?",
                "MX": "\\d{5}",
                "MD": "\\d{4}",
                "MC": "980\\d{2}",
                "MA": "\\d{5}",
                "NP": "\\d{5}",
                "NZ": "\\d{4}",
                "NI": "((\\d{4}-)?\\d{3}-\\d{3}(-\\d{1})?)?",
                "NG": "(\\d{6})?",
                "OM": "(PC )?\\d{3}",
                "PK": "\\d{5}",
                "PY": "\\d{4}",
                "PH": "\\d{4}",
                "PL": "\\d{2}-\\d{3}",
                "PR": "00[679]\\d{2}([ \-]\\d{4})?",
                "RO": "\\d{6}",
                "RU": "\\d{6}",
                "SM": "4789\\d",
                "SA": "\\d{5}",
                "SN": "\\d{5}",
                "SK": "\\d{3}[ ]?\\d{2}",
                "SI": "\\d{4}",
                "ZA": "\\d{4}",
                "LK": "\\d{5}",
                "TJ": "\\d{6}",
                "TH": "\\d{5}",
                "TN": "\\d{4}",
                "TR": "\\d{5}",
                "TM": "\\d{6}",
                "UA": "\\d{5}",
                "UY": "\\d{5}",
                "UZ": "\\d{6}",
                "VA": "00120",
                "VE": "\\d{4}",
                "ZM": "\\d{5}",
                "AS": "96799",
                "CC": "6799",
                "CK": "\\d{4}",
                "RS": "\\d{6}",
                "ME": "8\\d{4}",
                "CS": "\\d{5}",
                "YU": "\\d{5}",
                "CX": "6798",
                "ET": "\\d{4}",
                "FK": "FIQQ 1ZZ",
                "NF": "2899",
                "FM": "(9694[1-4])([ \-]\\d{4})?",
                "GF": "9[78]3\\d{2}",
                "GN": "\\d{3}",
                "GP": "9[78][01]\\d{2}",
                "GS": "SIQQ 1ZZ",
                "GU": "969[123]\\d([ \-]\\d{4})?",
                "GW": "\\d{4}",
                "HM": "\\d{4}",
                "IQ": "\\d{5}",
                "KG": "\\d{6}",
                "LR": "\\d{4}",
                "LS": "\\d{3}",
                "MG": "\\d{3}",
                "MH": "969[67]\\d([ \-]\\d{4})?",
                "MN": "\\d{6}",
                "MP": "9695[012]([ \-]\\d{4})?",
                "MQ": "9[78]2\\d{2}",
                "NC": "988\\d{2}",
                "NE": "\\d{4}",
                "VI": "008(([0-4]\\d)|(5[01]))([ \-]\\d{4})?",
                "PF": "987\\d{2}",
                "PG": "\\d{3}",
                "PM": "9[78]5\\d{2}",
                "PN": "PCRN 1ZZ",
                "PW": "96940",
                "RE": "9[78]4\\d{2}",
                "SH": "(ASCN|STHL) 1ZZ",
                "SJ": "\\d{4}",
                "SO": "\\d{5}",
                "SZ": "[HLMS]\\d{3}",
                "TC": "TKCA 1ZZ",
                "WF": "986\\d{2}",
                "XK": "\\d{5}",
                "YT": "976\\d{2}"
            },
            postalRule: function(postal, countryCode){
                console.log("POSTAL", postal, "COUNTRYCODE", countryCode);
                console.log("REGEXP", new RegExp("^" + this.postals[countryCode] + "$"));
                console.log("match", postal.match(new RegExp("^" + this.postals[countryCode] + "$")));
                return postal.match(new RegExp("^" + this.postals[countryCode] + "$")) === null;
            },
            peselRule: function(pesel){
                if (pesel.length == 11)
                {
                    var numsArr = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
                    var checksum = 0;
                    for (var i = 0; i < 10; i++)
                    {
                        checksum += numsArr[i] * pesel[i];
                    }
                    checksum = (checksum%10) === 0 ? 0 : 10 - checksum % 10;
                    console.log("Checksum ", checksum);
                    console.log("pesel[10] ", pesel[10]);
                    return checksum === Number(pesel[10]);
                }
                else
                {
                    return false;
                }
            },
            ibanRule: function(iban){
                console.log("ValidIban", this.isValidIBANNumber(iban));
                if(this.isValidIBANNumber(iban) !== 1){
                    return false;
                }
                else{
                    return true;
                }
            },
            regonRule: function(regon){
                var WEIGHTS = [8, 9, 2, 3, 4, 5, 6, 7];
                var MODULO = 11;
                if (typeof regon === 'string' && regon.length === 9) {
                    var i, sum = 0, checksum, digits = [];
                    for (i = 0; i < WEIGHTS.length; i++) {
                        digits.push(+regon[i]);
                        sum += digits[i] * WEIGHTS[i];
                    }
                    checksum = sum % MODULO;
                    return checksum === (+regon[8]);
                }
                return false;
            },
            isValidIBANNumber: function(input) {
                var CODE_LENGTHS = {
                    AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
                    CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
                    FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
                    HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
                    LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
                    MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
                    RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
                };
                var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
                    code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
                    digits;
                // check syntax and length
                if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
                    return false;
                }
                // rearrange country code and check digits, and convert chars to ints
                digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
                    return letter.charCodeAt(0) - 55;
                });
                // final check
                return this.mod97(digits);
            },
            mod97: function(string) {
                var checksum = string.slice(0, 2), fragment;
                for (var offset = 2; offset < string.length; offset += 7) {
                    fragment = String(checksum) + string.substring(offset, offset + 7);
                    checksum = parseInt(fragment, 10) % 97;
                }
                return checksum;
            },
            phone: function(tel) {
                var reg = /^[0-9\+]{8,13}$/;
                if(reg.test(tel) == false) {
                    return false;
                }
                else{
                    return true;
                }
            },
            urlRule: function(str) {
                if(!/^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/.test(str)) {
                    alert("Please enter a valid URL.");
                    return false;
                } else {
                    return true;
                }
            },
            checkUsername: function(username, property, xeditable, UserService) {
                var deferred = $q.defer();
                if (username) {
                    if (typeof username !== 'string') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if (username.length < 6) {
                        !xeditable && property.$setValidity("tooShort", false);
                        deferred.resolve('Nazwa użytkownika musi mieć co najmniej 6 znaków');
                    }
                    else {
                        UserService.checkUsername({'username': username}).then(function (data) {
                            if (!data) {
                                if (!xeditable) {
                                    property.$setValidity("required", true);
                                    property.$setValidity("tooShort", true);
                                    property.$setValidity("nameBusy", true);
                                    deferred.resolve('OK')
                                }
                                else{
                                    deferred.resolve(data);
                                }
                            }
                            else {
                                !xeditable && property.$setValidity("nameBusy", false);
                                deferred.resolve(data);
                            };
                        });
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkEmail: function(email, property, xeditable,UserService){
                var deferred = $q.defer();
                if (email) {
                    if (typeof email !== 'string' || email === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if (email.match(this.emailFormat) === null) {
                        !xeditable && property.$setValidity("emailFormat", false);
                        deferred.resolve("Niepoprawny adres email");
                    }
                    else {
                        UserService.checkEmail({'email': email}).then(function(data){
                            if (!data) {
                                if (!xeditable) {
                                    property.$setValidity("required", true);
                                    property.$setValidity("emailFormat", true);
                                    property.$setValidity("emailBusy", true);
                                    deferred.resolve('OK')
                                }
                                else{
                                    deferred.resolve(data);
                                }
                            }
                            else {
                                !xeditable && property.$setValidity("emailBusy", false);
                                deferred.resolve(data);
                            };
                        });
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkDisplayName: function(display_name, property, xeditable){
                var deferred = $q.defer();
                if(display_name) {
                    if (typeof display_name !== 'string' || display_name === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkPassword: function (password, property, xeditable) {
                var deferred = $q.defer();
                if(password){
                    if (typeof password !== 'string') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if(password.length < 6) {
                        !xeditable && property.$setValidity("tooShort", false);
                        deferred.resolve('Hasło musi mieć co najmniej 6 znaków');
                    }
                    else {
                        if(!xeditable){
                            property.$setValidity("required", true);
                            property.$setValidity("tooShort", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkOldPassword: function (oldPassword, property, xeditable) {
                var deferred = $q.defer();
                if(oldPassword){
                    if (typeof oldPassword !== 'string') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if(oldPassword.length < 6) {
                        !xeditable && property.$setValidity("tooShort", false);
                        deferred.resolve('Hasło musi mieć co najmniej 6 znaków');
                    }
                    else {
                        if(!xeditable){
                            property.$setValidity("required", true);
                            property.$setValidity("tooShort", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkPasswordValidation: function (passwordValidation, password, property, xeditable) {
                var deferred = $q.defer();
                if (!passwordValidation || typeof passwordValidation !== 'string') {
                    !xeditable && property.$setValidity("required", false);
                    deferred.resolve('Pole wymagane');
                }
                else if(passwordValidation !== password) {
                    !xeditable && property.$setValidity("passwordConfirmation", false);
                    deferred.resolve('Pole musi być identyczne z powyższym');
                }
                else {
                    if(!xeditable){
                        !xeditable && property.$setValidity("required", true);
                        !xeditable && property.$setValidity("passwordConfirmation", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            checkPesel: function(pesel, property, xeditable){
                var deferred = $q.defer();
                if (!pesel || typeof pesel !== 'string') {
                    if(!xeditable){
                        property.$setValidity("peselFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                else if(!this.peselRule(pesel)) {
                    !xeditable && property.$setValidity("peselFormat", false);
                    deferred.resolve('Błędny numer PESEL');
                }
                else {
                    if(!xeditable){
                        !xeditable && property.$setValidity("peselFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            checkCompanyFullName: function(fullName, proprty, xeditable){
                var deferred = $q.defer();
                if(fullName) {
                    if (typeof fullName !== 'string' || fullName === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkCompanyName: function(name, proprty, xeditable){
                var deferred = $q.defer();
                if(name) {
                    if (typeof name !== 'string' || name === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkNip: function(nip, property, xeditable, CompanyService){
                var deferred = $q.defer();
                if (nip) {
                    if (typeof nip !== 'string' || nip === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if (nip.match(this.nipFormat) === null) {
                        !xeditable && property.$setValidity("nipFormat", false);
                        deferred.resolve("Niepoprawny numer NIP");
                    }
                    else {
                        CompanyService.checkNip({'nip': nip}).then(function(data){
                            if (!data) {
                                if (!xeditable) {
                                    property.$setValidity("required", true);
                                    property.$setValidity("nipFormat", true);
                                    property.$setValidity("nipBusy", true);
                                    deferred.resolve('OK')
                                }
                                else{
                                    deferred.resolve(data);
                                }
                            }
                            else {
                                !xeditable && property.$setValidity("nipBusy", false);
                                deferred.resolve(data);
                            };
                        });
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkIban: function(iban, property, xeditable, UserService){
                var deferred = $q.defer();
                if (iban) {
                    if (typeof iban !== 'string' || iban === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else if (!this.ibanRule(iban)) {
                        !xeditable && property.$setValidity("ibanFormat", false);
                        deferred.resolve("Niepoprawny numer konta bankowego");
                    }
                    else {
                        UserService.checkIban({'iban': iban}).then(function(data){
                            if (!data) {
                                if (!xeditable) {
                                    property.$setValidity("required", true);
                                    property.$setValidity("ibanFormat", true);
                                    property.$setValidity("ibanBusy", true);
                                    deferred.resolve('OK')
                                }
                                else{
                                    deferred.resolve(data);
                                }
                            }
                            else {
                                !xeditable && property.$setValidity("ibanBusy", false);
                                deferred.resolve(data);
                            };
                        });
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkRegon: function(regon, property, xeditable){
                var deferred = $q.defer();
                if (!regon || typeof regon !== 'string') {
                    if(!xeditable){
                        property.$setValidity("regonFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                else if(!this.regonRule(regon)) {
                    !xeditable && property.$setValidity("regonFormat", false);
                    deferred.resolve('Błędny numer REGON');
                }
                else {
                    if(!xeditable){
                        property.$setValidity("regonFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            checkAddress: function(address, property, xeditable){
                var deferred = $q.defer();
                if(address) {
                    if (typeof address !== 'string' || address === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkPostal: function(postal, property, xeditable, countryCode){
                var deferred = $q.defer();
                if (!postal || typeof postal !== 'string') {
                    if(!xeditable){
                        property.$setValidity("postalFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                else if(this.postalRule(postal, countryCode || "PL")) {
                    !xeditable && property.$setValidity("postalFormat", false);
                    deferred.resolve('Błędny kod pocztowy');
                }
                else {
                    if(!xeditable){
                        !xeditable && property.$setValidity("postalFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            checkCommune: function(commune, property, xeditable){
                var deferred = $q.defer();
                if(commune) {
                    if (typeof commune !== 'string' || commune === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkCity: function(city, poperty, xeditable){
                var deferred = $q.defer();
                if(city) {
                    if (typeof city !== 'string' || city === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkIbanName: function(ibanName, property, xeditable){
                var deferred = $q.defer();
                if(ibanName) {
                    if (typeof ibanName !== 'string' || ibanName === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkPhone: function(phone, property, xeditable){
                var deferred = $q.defer();
                if (!phone || typeof phone !== 'string') {
                    if(!xeditable){
                        property.$setValidity("phoneFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                else if(!this.phone(phone)) {
                    !xeditable && property.$setValidity("phoneFormat", false);
                    deferred.resolve('Błędny numer telefonu');
                }
                else {
                    if(!xeditable){
                        !xeditable && property.$setValidity("phoneFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            checkFullName: function(fullName, proprty, xeditable){
                var deferred = $q.defer();
                if(fullName) {
                    if (typeof fullName !== 'string' || fullName === '') {
                        !xeditable && property.$setValidity("required", false);
                        deferred.resolve('Pole wymagane');
                    }
                    else{
                        if (!xeditable) {
                            !xeditable && property.$setValidity("required", true);
                            deferred.resolve('OK');
                        }
                        else{
                            deferred.resolve();
                        }
                    }
                }
                else{
                    deferred.reject();
                }
                return deferred.promise;
            },
            checkUrl: function(url, property, xeditable){
                var deferred = $q.defer();
                if (!url || typeof url !== 'string') {
                    if(!xeditable){
                        property.$setValidity("urlFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                else if(!this.urlRule(url)) {
                    !xeditable && property.$setValidity("urlFormat", false);
                    deferred.resolve('Błędny adres www');
                }
                else {
                    if(!xeditable){
                        !xeditable && property.$setValidity("urlFormat", true);
                        deferred.resolve('OK');
                    }
                    else{
                        deferred.resolve();
                    }
                }
                return deferred.promise;
            },
            
        }
    });