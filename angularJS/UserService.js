'use strict';
/**
 * @ngdoc function
 * @name firstOfficeApp.factory:UserService
 * @description
 * # UserService
 * Service of the firstOfficeApp
 */
angular.module('firstOfficeApp')
    .factory('UserService', function($window, $state, BaseService) {
        var basePath = '/api/users/';
        return {
            getUserData: function() {
                return BaseService.makeGetRequest(basePath + 'user-data');
            },
            getPersonalUserData: function() {
                return BaseService.makeGetRequest(basePath + 'personal-user-data');
            },
            updateUser: function(userData) {
                return BaseService.makePostRequest(basePath + 'update-user', userData);
            },
            updateUserPersonal: function(personalUserData){
                return BaseService.makePostRequest(basePath + 'update-user-personal', personalUserData);
            },
            logIn: function(loginData){
                return BaseService.makePostRequest(basePath + 'login', loginData);
            },
            changePassword: function(data){
                return BaseService.makePostRequest(basePath + 'change-password', data);
            },
            setToken: function(access_token){
                $window.sessionStorage.access_token = access_token
            },
            logout: function () {
                delete $window.sessionStorage.access_token;
                $state.go('login');
            },
            getRoles: function(){
                return BaseService.makeGetRequest(basePath + 'export-roles');
            },
            getUsers: function(){
                return BaseService.makeGetRequest(basePath + 'index');
            },
            createUser: function(data){
                return BaseService.makePostRequest(basePath + 'create', data);
            },
            getCountries: function(){
                return BaseService.makeStaticFileRequest('/js/countries.json');
            },
            checkUsername: function(data){
                return BaseService.makeValidationRequest(basePath + 'check-username', data);
            },
            checkEmail: function(data){
                return BaseService.makeValidationRequest(basePath + 'check-email', data);
            },
            getIbans: function(){
                return BaseService.makeGetRequest(basePath + 'ibans');
            },
            checkIban: function(data){
                return BaseService.makeValidationRequest(basePath + 'check-iban', data);
            },
            addIban: function(data){
                data.number = data.number.toUpperCase();
                data.number = data.number.replace(/\s+/g, '');
                data.number = data.number.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
                return BaseService.makePostRequest(basePath + 'add-iban', data);
            },
            setDefaultIban: function(ibanId){
                return BaseService.makePostRequest(basePath + 'set-default-iban', {'iban_id': ibanId});
            },
            getInvoiceFormat: function(){
                return BaseService.makeGetRequest(basePath + 'invoice-format');
            },
            updateInvoiceFormat: function(data){
                return BaseService.makePostRequest(basePath + 'update-invoice-format', data);
            },
            getUserMailData: function(){
                return BaseService.makeGetRequest(basePath + 'user-mail');
            },
            updateUserMailData: function(data){
                return BaseService.makePostRequest(basePath + 'update-user-mail', data);
            },
            getUserConfig: function(){
                return BaseService.makeGetRequest(basePath + 'user-config');
            },
            updateUserConfig: function(data){
                return BaseService.makePostRequest(basePath + 'update-user-config', data);
            },
        }
    });