'use strict';
/**
 * @ngdoc function
 * @name firstOfficeApp.controller:UserController
 * @description
 * # UserController
 * Controller of the firstOfficeApp
 */
angular.module('firstOfficeApp')
    .controller('UserController', ['$scope', '$timeout', 'UserService', 'CompanyService', '$state', '$filter', 'Validators', 'FileUploader', '$window', 'FileUtils', function ($scope, $timeout, UserService, CompanyService, $state, $filter, Validators, FileUploader, $window, FileUtils) {
        //Controller data section
        $scope.data = {};
        $scope.data.user = {};
        $scope.data.userPersonal = {};
        $scope.data.passwordReset = {};
        $scope.data.newUser = {};
        $scope.data.company = {};
        $scope.data.gus = {identifier: 'nip'};
        $scope.data.ibans = [];
        $scope.data.newIban = {};
        $scope.data.invoiceNumber = {};
        $scope.data.userMailAddress = {};
        $scope.data.userMailContact = {};
        $scope.data.userConfig = {};
        //Controller properties section
        $scope.messages = {
            username: 'Pole wymagane',
            display_name: 'Pole wymagane',
            email: 'Pole wymagane',
            password: 'Pole wymagane',
            passwordConfirmation: 'Pole wymagane',
            oldPassword: 'Pole wymagane',
            iban: 'Pole wymagane',
            ibanName: 'Pole wymagane'
        };
        $scope.invoiceNumbers = [
            {formatString: 'N', text: "Numer bez wypełniania zerami"},
            {formatString: 'n2', text: "Numer wypełniony zerami do dwóch pozycji"},
            {formatString: 'n3', text: "Numer wypełniony zerami do trzech pozycji"},
            {formatString: 'n4', text: "Numer wypełniony zerami do czterech pozycji"},
            {formatString: 'n5', text: "Numer wypełniony zerami do pięciu pozycji"},
        ];
        $scope.invoiceMonths = [
            {formatString: 'M', text: "Miesiąc bez wypełniania do dwóch pozycji"},
            {formatString: 'm', text: "Misiąc z wypełnianiem do dwóch pozycji"},
            {formatString: 'N', text: "Brak numeracji miesięcznej"},
        ];
        $scope.invoiceYears = [
            {formatString: 'R', text: "Pełen rok"},
            {formatString: 'r', text: "Tylko dwie ostatnie cyfry roku"},
        ];
        $scope.countries = [];
        $scope.gus = false;
        $scope.captchaUrl = 'storage/tmp/captcha.jpeg?' +(new Date().getTime()).toString();
        $scope.editingLogo = true;
        $scope.logoPath = '';
        $scope.addingIban = false;
        $scope.editingCompany = false;
        $scope.logoUploader = new FileUploader({
            url: '/api/companies/update-logo',
            headers: {
                'Authorization': 'Bearer ' + $window.sessionStorage.access_token
            },
            queueLimit: 1,
            autoUpload: true,
            filters: [{
                name: 'imageFilter',
                fn: function(item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|'.indexOf(type) !== -1;
                }
            }],
            onCompleteItem: function(item, response, status, headers){
                $scope.data.company.logo = response.data.originalPath;
            },
            onCompleteAll: function(){
                $scope.editingLogo = false;
                $scope.logoPath = $scope.getLogoUrl();
                $scope.logoUploader.clearQueue();
            }
        });
        //Controller methods section 
        UserService.getUserData().then(function(data){
            $scope.data.user = data;
        });
        UserService.getPersonalUserData().then(function(data){
            $scope.data.userPersonal = data;
        });
        UserService.getInvoiceFormat().then(function(data){
            $scope.data.invoiceNumber = $scope.splitInvoiceFormat(data);
        });
        UserService.getUserMailData().then(function(data){
            $scope.data.userMailAddress = data;
            $scope.data.userMailContact = data;
        });
        UserService.getUserConfig().then(function(data){
            $scope.data.userConfig = data;
        });
        CompanyService.userCompany().then(function(data){
            $scope.data.company = data;
            if(!data.logo || data.logo === ''){
                $scope.editingLogo = true;
            }
            else{
                $scope.editingLogo = false;
                $scope.logoPath = $scope.getLogoUrl();
            }
        });
        UserService.getCountries().then(function(data) {
            $scope.countries = data;
        });
        UserService.getIbans().then(function(data) {
            $scope.data.ibans = data;
        });

        $scope.fromGus = function(){
            var data = {};
            data[$scope.data.gus.identifier] = $scope.data.gus.value;
            data['captcha'] = $scope.data.gus.captcha;
            CompanyService.fromGus(data).then(function(data){
                $scope.gus = false;
                $scope.data.company = data;
            });
        };
        $scope.logout = function(){
            UserService.logout();
        };
        $scope.updateUser = function(){
            UserService.updateUser($scope.data.user).then(function(data){
                $scope.data.user = data;
            });
        };
        $scope.updateUserPersonal = function(){
            UserService.updateUserPersonal($scope.data.userPersonal).then(function(data){
               $scope.data.userPersonal = data;
            });

        };
        $scope.checkUsername = function(username, formReference){
            Validators.checkUsername(username, formReference, false, UserService).then(function(message){
                $scope.messages.username = message;
            });
        };
        $scope.checkUsernameEdit = function(username){
            return Validators.checkUsername(username, null, true, UserService);
        };
        $scope.checkDisplayName = function(display_name, formReference){
            Validators.checkDisplayName(display_name, formReference, false).then(function(message){
                $scope.messages.display_name = message;
            });
        };
        $scope.checkDisplayNameEdit = function(display_name){
            return Validators.checkDisplayName(display_name, null, true);
        };
        $scope.checkEmail = function(email, formReference){
            Validators.checkEmail(email, formReference, false, UserService).then(function(message){
                $scope.messages.email = message;
            });
        };
        $scope.checkEmailEdit = function(email){
            return Validators.checkEmail(email, null, true, UserService);
        };
        $scope.checkPassword = function(password, formReference){
            Validators.checkPassword(password, formReference, false).then(function(message){
                $scope.messages.password = message;
            });
        };
        $scope.checkOldPassword = function(oldPassword, formReference){
            Validators.checkOldPassword(oldPassword, formReference, false).then(function(message){
                $scope.messages.oldPassword = message;
            });
        };
        $scope.checkPasswordConfirmation = function(passwordValidation,password, formReference){
            Validators.checkPasswordValidation(passwordValidation, password, formReference, false).then(function(message){
                $scope.messages.passwordConfirmation = message;
            });
        };
        $scope.checkComapnyFullNameEdit = function(fullName){
           return  Validators.checkCompanyFullName(fullName, null, true);
        };
        $scope.checkComapnyNameEdit = function(name){
            return  Validators.checkCompanyName(name, null, true);
        };
        $scope.checkPeselEdit = function(pesel){
            return Validators.checkPesel(pesel, null, true);
        };
        $scope.checkNipEdit = function(nip){
            return Validators.checkNip(nip, null, true, CompanyService);
        };
        $scope.checkRegonEdit = function(regon){
            return Validators.checkRegon(regon, null, true);
        };
        $scope.checkAddressEdit = function(address){
            return Validators.checkAddress(address, null, true);
        };
        $scope.checkPostalEdit = function(postal, countryCode){
            return Validators.checkPostal(postal, null, true, countryCode);
        };
        $scope.checkCommuneEdit = function(commune){
            return Validators.checkCommune(commune, null, true);
        };
        $scope.checkCityEdit = function(city){
            return Validators.checkCity(city, null, true);
        };
        $scope.checkIban = function(iban, formReference){
            Validators.checkIban(iban, formReference, false, UserService).then(function(message){
                $scope.messages.iban = message;
            });
        };
        $scope.checkIbanName = function(ibanName, formReference){
            Validators.checkIbanName(ibanName, formReference, false).then(function(message){
                $scope.messages.ibanName = message;
            });
        };
        $scope.changePassword = function(){
            UserService.changePassword($scope.data.passwordReset).then(function(data){
                try {
                    $state.go($rootScope.lastState);
                }
                catch(e){
                    $state.go('dashboard.home');
                }
            });
        };
        $scope.createUser = function(){
            UserService.createUser($scope.data.newUser).then(function(data){
                try {
                    $state.go($rootScope.lastState);
                }
                catch(e){
                    $state.go('dashboard.users')
                }
            });
        };
        $scope.showCountry = function() {
            if($scope.countries.length > 0) {
                if ($scope.data.company.country) {
                    var selected = $filter('filter')($scope.countries, {code: $scope.data.company.country});
                    return selected[0].name;
                } else {
                    var selected = $filter('filter')($scope.countries, {code: 'PL'});
                    return (selected[0] && angular.isObject(selected[0])) ? selected[0].name : '';
                }
            }
        };
        $scope.loadCountries = function() {
            return $scope.countries.length ? null : UserService.getCountries().then(function(data) {
                $scope.countries = data;
            });
        };
        $scope.updateCompany = function(){
            $scope.data.company.user_company = true;
            CompanyService.update($scope.data.company).then(function(data){
                $scope.data.company = data;
            });
        };
        $scope.updateInvoiceNumber = function(){
            UserService.updateInvoiceFormat($scope.buildInvoiceFormat()).then(function(data){
                $scope.data.inviceNumber = data;
            });
        };
        $scope.showGus = function(){
            $scope.gus = true;
        };
        $scope.changeLogo = function(){
            $scope.editingLogo = true;
        };
        $scope.cancelChangeLogo = function(){
            $scope.editingLogo = false;
        };
        $scope.getLogoUrl = function(){
            return FileUtils.getBigThumbnailPath($scope.data.company.logo)
        };
        $scope.hideGus = function(){
            $scope.gus = false;
        };
        $scope.showAddIban = function(){
            $scope.addingIban = true;
        };
        $scope.hideAddIban = function(){
            $scope.addingIban = false;
        };
        $scope.editCompany = function(){
            $scope.editingCompany = true;
        };
        $scope.stopEditingCompany = function(){
            $scope.editingCompany = false;
        };
        $scope.addIban = function(){
            $scope.data.newIban.default = ($scope.data.ibans.length === 0) ? 1 : 0;
            UserService.addIban($scope.data.newIban).then(function(data){
                $scope.addingIban = false;
                UserService.getIbans().then(function(data) {
                    $scope.data.ibans = data;
                    $scope.data.newIban = {};
                });
            })
        };
        $scope.setDeafultIban = function (ibanId) {
            UserService.setDefaultIban(ibanId).then(function() {
                UserService.getIbans().then(function (data) {
                    $scope.data.ibans = data;
                });
            });
        };
        $scope.buildInvoiceFormat = function(){
            return {invoiceFormat: $scope.data.invoiceNumber.prefix + " " + $scope.data.invoiceNumber.number + " " + $scope.data.invoiceNumber.subfix + " " + $scope.data.invoiceNumber.month + " " + $scope.data.invoiceNumber.year + " " + $scope.data.invoiceNumber.postfix};
        };
        $scope.splitInvoiceFormat = function(data){
            var raw = data.invoiceFormat ? data.invoiceFormat.split(" ") : [];
            var ret = {
                prefix: '',
                number: 'N',
                subfix: '',
                month: 'N',
                year: 'R',
                postfix: ''
            };
            if(raw.length === 6){
                ret.prefix = raw[0];
                ret.number = raw[1];
                ret.subfix = raw[2];
                ret.month = raw[3];
                ret.year = raw[4];
                ret.postfix = raw[5];
            };
            return ret;
        };
        $scope.updateUserMailAddress = function(){
            console.log("User mail data at updating: ", $scope.data.userMail);
            UserService.updateUserMailData($scope.data.userMailAddress).then(function(data){
                $scope.data.userMailAddress = data;
                $scope.data.userMailContact = data;
            });
        };
        $scope.updateUserMailContact = function(){
            UserService.updateUserMailData($scope.data.userMailContact).then(function(data){
                $scope.data.userMailContact = data;
                $scope.data.userMailAddress = data;
            });
        };
        $scope.checkPhoneEdit = function(phone){
            return Validators.checkPhone(phone, null, true);
        };
        $scope.checkFullNameEdit = function(fullName){
            return  Validators.checkFullName(fullName, null, true);
        };
        $scope.updateLawBase = function(){
            CompanyService.updateLawBase($scope.data.company).then(function(data){
                $scope.data.company = data;
            });
        };
        $scope.checkUrl = function(url){
            return Validators.checkUrl(url, null, true);
        };
        $scope.updateUserConfig = function(){
            UserService.updateUserConfig($scope.data.userConfig).then(function(data){
                $scope.data.userConfig = data;
            });
        };

    }]);