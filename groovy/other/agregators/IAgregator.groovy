package anteeo.demeter.data.agregators

import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 5/31/16.
 * Interface to determine type for all Agregator classes
 */
@CompileStatic
interface IAgregator {

    /**
     * Method which calculates BigDecimal value from given data set using algorithm defined by Agregator class
     * @param data
     * @return
     */
    public BigDecimal calculate(List<ReducedMeteringPointValue> data)

    /**
     * Returns Status of agregated value, returns integer value of ReducedMeteringPointValue.DATA_STATE ENUM
     * @return
     */
    public Integer getStatus()

    /**
     * Returns timestamp of agregated value depending of time calculation algorithm
     * @param begin
     * @param end
     * @return
     */
    public Long getTime(Long begin, Long end)
}
