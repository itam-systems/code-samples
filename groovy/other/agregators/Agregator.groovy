package anteeo.demeter.data.agregators

import groovy.transform.CompileStatic
import org.apache.log4j.Logger

/**
 * Created by Piotr Czubek on 6/1/16.
 * Base class for all calculation methods
 */
@CompileStatic
class Agregator {
    public Logger log = Logger.getLogger(getClass())

    protected Integer status
    protected Integer timeMethod = TIME_CALCULATION.END.getValue()

    /**
     * ENUM which is used to determine how calculation alghoritms should work with timestamps
     * END returns timestamp of last value
     * BEGIN returns timestamp of first value
     * AVG returns timestamp in the middle between last and first value
     */
    public static final enum TIME_CALCULATION {
        END(0), BEGIN(1), AVG(2)
        private final int value

        private TIME_CALCULATION(int value) {
            this.value = value
        }

        public int getValue() {
            return value
        }
    }

    /**
     * Returns status of agregation effect as value of ENUM ReducedMeteringPointValue.DATA_STATE
     * @return
     */
    public Integer getStatus(){
        return this.status
    }

    /**
     * Sets status of agregation, should be used only on special cases
     * Generally status is defined automatically on agregation process
     * Takes Integer value of ReducedMeteringPointValue.DATA_STATE ENUM field
     * @param status
     */
    public void setStatus(Integer status){
        this.status = status
    }

    /**
     * Defines method of time calculation as value of TIME_CALCULATION ENUM field
     * @param timeMethod
     */
    public void setTimeMethod(Integer timeMethod){
        this.timeMethod = timeMethod
    }

    /**
     * Returns integer value of time calculation method
     * @return
     */
    public Integer getTimeMethod(){
        return this.timeMethod
    }

    /**
     * Sets agregation algorithm defined as Class of type IAgregator
     * @param agregator
     */
    public void setAgregator(IAgregator agregator){
        this.agregator = agregator
    }

    /**
     * Returns agregation algorithm as Class of type IAgregator
     * @return
     */
    public IAgregator getAgregator(){
        return this.agregator
    }

    /**
     * Returns timestamp for pair begin and end depending on defined time calculation algorithm
     * @param begin
     * @param end
     * @return
     */
    public Long getTime(Long begin, Long end){
        try {
            if (this.timeMethod == TIME_CALCULATION.END.value) {
                return end
            } else if (this.timeMethod == TIME_CALCULATION.BEGIN.value) {
                return begin
            } else {
                return begin + ((end - begin)/2).toLong()
            }
        }
        catch(Exception e){
            log.error "Exception when counting data timestamp: ${e.message}"
            log.error e.stackTrace
        }
    }
}
