package anteeo.demeter.data.agregators

import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 5/31/16.
 * Avarage agregator, agregates data set to single avarage value
 * If value from data set is null returns incomplete state
 */
@CompileStatic
class AvgAgregator extends Agregator implements IAgregator {

    /**
     * Calculates, and returns as BigDecimal avarage value of given data set
     * @param data
     * @return
     */
    public BigDecimal calculate(List<ReducedMeteringPointValue> data){
        this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        BigDecimal val
        BigDecimal buff
        try {
            for (ReducedMeteringPointValue dataEntry : data) {
                if(dataEntry != null && dataEntry.val != null) {
                    if(dataEntry.status == ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue() || dataEntry.status == ReducedMeteringPointValue.DATA_STATE.GAP.getValue()){
                        this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                    }
                    if(buff != null) {
                        buff += dataEntry.val
                    }
                    else{
                        buff = dataEntry.val
                    }
                }
                else{
                    this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                }
            }
            if(buff != null) {
                val = (BigDecimal) buff / data.size()
            }
        }
        catch(Exception e){
            this.log.error "AvgAgregator throws an exception on calculation execution"
            this.log.error e.message
        }
        if(val ==null){
            this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        }
        else if(val != null && status != ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()){
            this.status = ReducedMeteringPointValue.DATA_STATE.OK.getValue()
        }
        return val
    }
}
