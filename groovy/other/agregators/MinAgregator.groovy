package anteeo.demeter.data.agregators

import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 5/31/16.
 * Min Agregator, agregates data set to single minimal value
 * If any of data set values is null it sets incomplete status
 */
@CompileStatic
class MinAgregator extends Agregator implements IAgregator{

    /**
     * Calculates and returns as BigDecimal minimal value in given data set
     * @param data
     * @return
     */
    public BigDecimal calculate(List<ReducedMeteringPointValue> data){
        this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        BigDecimal val
        try {
            for (ReducedMeteringPointValue dataEntry : data) {
                if(dataEntry != null && dataEntry.val != null) {
                    if(dataEntry.status == ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue() || dataEntry.status == ReducedMeteringPointValue.DATA_STATE.GAP.getValue()){
                        this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                    }
                    if (val == null || dataEntry.val < val) {
                        val = dataEntry.val
                    }
                }
                else{
                    this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                }
            }
        }
        catch(Exception e){
            this.log.error "MinAgregator throws an exception on calculation execution"
            this.log.error e.message
        }
        if(val ==null){
            this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        }
        else if(val != null && status != ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()){
            this.status = ReducedMeteringPointValue.DATA_STATE.OK.getValue()
        }
        return val
    }
}
