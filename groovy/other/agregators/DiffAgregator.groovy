package anteeo.demeter.data.agregators

import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 5/31/16.
 * Diff Agregator, agregates data set to single difference value
 * Returns incomplete state if any terminal value is null
 */
@CompileStatic
class DiffAgregator extends Agregator implements IAgregator{

    /**
     * Calculates and returns as BigDecimal difference between first and last value of data set
     * @param data
     * @return
     */
    public BigDecimal calculate(List<ReducedMeteringPointValue> data){
        this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        BigDecimal val
        try {
            ReducedMeteringPointValue begin
            ReducedMeteringPointValue end
            if(data.first() != null && data.first().val != null){
                if(data.first().status == ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue() || data.first().status == ReducedMeteringPointValue.DATA_STATE.GAP.getValue()){
                    this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                }
                begin = data.first()
            }
            else{
                begin = data.find{it && it.val}
                this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
            }
            if(data.last() != null && data.last().val != null){
                if(data.last().status == ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue() || data.last().status == ReducedMeteringPointValue.DATA_STATE.GAP.getValue()){
                    this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
                }
                end = data.last()
            }
            else{
                end = lastNotNull(data)
                this.status = ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()
            }

            val = end.val - begin.val
        }
        catch(Exception e){
            this.log.error "DiffAgregator throws an exception on calculation execution"
            this.log.error e.message
        }
        if(val ==null){
            this.status = ReducedMeteringPointValue.DATA_STATE.GAP.getValue()
        }
        else if(val != null && status != ReducedMeteringPointValue.DATA_STATE.INCOMPLETE.getValue()){
            this.status = ReducedMeteringPointValue.DATA_STATE.OK.getValue()
        }
        return val
    }

    private ReducedMeteringPointValue lastNotNull(List<ReducedMeteringPointValue> data){
        for(int i = data.size() -1; i >= 0;i--){
            if(data[i] != null && data[i].val != null){
                return data[i]
            }
        }
    }
}
