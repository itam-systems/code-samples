package anteeo.demeter.data

import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 4/1/16.
 */
@CompileStatic
interface IReducedValuesManipulator {
    /**
     * Make a custom calculation determined by class on set of reduced metering point values lists determined by points group
     * saves calculation effect on class instance
     * @param mpValues
     */
    public void calculate(List<MeteringPointValuesInRange> mpValues)
    /**
     * Returns calculation effect
     * @return mpValues
     */
    public List<MeteringPointValuesInRange> getResult()
}
