package anteeo.demeter.data

import anteeo.demeter.data.agregators.Agregator
import anteeo.demeter.utils.Interval
import groovy.transform.CompileStatic
import groovy.transform.ToString

/**
 * Created by user on 4/1/16.
 */
@ToString(includeNames = true)
class ReducedMeteringPointValue{
    public static final enum DATA_STATE {
        OK(0), GAP(-1), INCOMPLETE(1), NOT_EXPECTED(2);

        private final int value;
        private DATA_STATE(int value) {
            this.value = value
        }

        public int getValue() {
            return value
        }
    }

    Long pointId
    Long time
    BigDecimal val
    Integer status


    static ReducedMeteringPointValue build(row) {
        if (!row)
            return null

        new ReducedMeteringPointValue(
                pointId: row.mpoint_id,
                time: row.time.getTime(),
                val: row.val,
                status: DATA_STATE.OK.getValue()
        )
    }
    static ReducedMeteringPointValue build(Long mpoint_id, Long time, BigDecimal val, Integer status = DATA_STATE.OK.getValue()) {
        new ReducedMeteringPointValue(
                pointId: mpoint_id,
                time: time,
                val: val,
                status: status
        )
    }

    public Long roundToInterval(Interval interval){
        Double intervalMultiplier = (this.time/interval.toMilis()).doubleValue()
        return (intervalMultiplier.round() * interval.toMilis())
    }

    public String toString(){
        String state  = ""
        if(this.status == DATA_STATE.OK){
            state = DATA_STATE.OK.name()
        }
        else if(this.status == DATA_STATE.INCOMPLETE.getValue()){
            state = DATA_STATE.INCOMPLETE.name()
        }
        else if (this.status == DATA_STATE.GAP.getValue()){
            state = DATA_STATE.GAP.name()
        }
        else if (this.status == DATA_STATE.NOT_EXPECTED.getValue()){
            state = DATA_STATE.NOT_EXPECTED.name()
        }

        return  "PointId: ${this.pointId}, Time: ${new Date(this.time)}, Status: ${state}, val: ${this.val}"
    }

}
