package anteeo.demeter.data

import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 4/7/16.
 */
@CompileStatic
class ToEnergy implements IReducedValuesManipulator{

    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>
    public ToEnergy(){
    }

    public void calculate(List<MeteringPointValuesInRange> mpValues){
        for(Integer pointIndex = 0; pointIndex < mpValues.size(); pointIndex++){
            MeteringPointValuesInRange entry = mpValues[pointIndex]
            MeteringPointValuesInRange tmpVals = entry.clone()
            tmpVals.manualCreated = (Boolean)true
            tmpVals.toEnergy()
            values.push(tmpVals)
        }
    }

    public List<MeteringPointValuesInRange> getResult(){
        return values
    }

}
