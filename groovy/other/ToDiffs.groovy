package anteeo.demeter.data

import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 4/7/16.
 */
@CompileStatic
class ToDiffs implements IReducedValuesManipulator{

    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>
    public ToDiffs(){
    }

    public void calculate(List<MeteringPointValuesInRange> mpValues){
        for(Integer pointIndex = 0; pointIndex < mpValues.size(); pointIndex++){
            MeteringPointValuesInRange entry = mpValues[pointIndex]
            MeteringPointValuesInRange tmpVals = entry.clone()
            tmpVals.manualCreated = (Boolean)true
            tmpVals.countDiffs()
            values.push(tmpVals)
        }
    }

    public List<MeteringPointValuesInRange> getResult(){
        return values
    }

}
