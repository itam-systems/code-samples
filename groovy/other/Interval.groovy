package anteeo.demeter.utils

import anteeo.demeter.MeteringPoint
import groovy.transform.CompileStatic

import java.lang.invoke.MethodHandleImpl

/**
 * Created by user on 3/16/16.
 */
@CompileStatic
class Interval {
    public static final long SEC = 1000
    public static final long MIN = 60000
    public static final long HOUR = 3600000
    public static final long QUARTER = 900000
    public static final long DAY = 86400000
    private Integer hours = 0
    private Integer days = 0
    private Integer minutes = 0
    private Integer seconds = 0


    Interval(long milisec){
        convert(milisec)
    }
    Interval(BigDecimal milisec){
        convert((long)milisec)
    }
    Interval(MeteringPoint point){
        if(point.cacheInterval > 0) {
            convert((long)point.cacheInterval)
        }
        else if(point.readInterval > 0){
            convert((long)point.readInterval)
        } else {
	    convert(SEC)
	}
    }
    public Boolean equalsWith(Interval with){
        return equals(this, with)
    }
    public static Boolean equals(Interval first, Interval second){
        return (first.days == second.days && first.hours == second.hours && first.minutes == second.minutes && first.seconds == second.seconds)
    }

    public static Boolean equals(List<Interval> intervals){
        List<Long> secondsList = [] as List<Long>
        for(interval in intervals){
            secondsList.push(interval.toseconds())
        }
        secondsList.unique(){Long a, Long b ->
            a <=> b
        }
        return (secondsList.size() == 1)
    }

    public static long getGcd(List<Interval> intervals){
        List<Long> secondsList = [] as List<Long>
        for(interval in intervals){
            secondsList.push(interval.toseconds())
        }
        secondsList.unique{ Long a, Long b ->
            a <=> b
        }
        long result = secondsList[0];
        if(secondsList.size() > 1) {
            for (int i = 1; i < secondsList.size(); i++) {
                result = gcd(result, secondsList[i])
            }
        }
        return result * 1000;
    }

    private void convert(long milisec) {

        if (milisec < 0 || milisec < SEC) return
        long d = (long) milisec/DAY
        if (d > 0) {
            milisec = milisec % DAY
            days = (Integer)d
        }

        long h = (long) milisec/HOUR
        if (h > 0) {
            milisec = milisec % HOUR
            hours = (Integer)h
        }

        long m = (long) milisec/MIN
        if (m > 0) {
            milisec = milisec % MIN
            minutes = (Integer)m
        }

        long s = (long) milisec/SEC
        if (s > 0) {
            seconds = (Integer)s
        }
    }

    public long toseconds(){
        return (long)((days * DAY + hours * HOUR + minutes * MIN + seconds * SEC)/1000)
    }
    public long toMilis(){
        return toseconds() * 1000
    }
    public BigDecimal toMinutes(){
        return (toseconds()/(long)60).toBigDecimal()
    }
    public BigDecimal toHours(){
        return (toMinutes()/(BigDecimal)60).toBigDecimal()
    }
    public BigDecimal toDays(){
        return (toHours()/(BigDecimal)24).toBigDecimal()
    }
    public static long gcd(long a, long b){
        while (b > 0)
        {
            long temp = b
            b = a % b
            a = temp;
        }
        return a
    }

    public BigDecimal calculateMultiplicationFactorToEnergy(){
        return toHours()
    }
    public BigDecimal calculateMultiplicationFactorToPower(){
        return 60/toMinutes()
    }
    public String toString(){
        String ret = ""
        if(days > 0){
            ret += "${days}d "
        }
        if(hours > 0){
            ret += "${hours}h "
        }
        if(minutes > 0){
            ret += "${minutes}m "
        }
        if(seconds > 0){
            ret += "${seconds}s "
        }
        else{
            if(ret.length() == 0){
                ret += "0s"
            }
        }
        return ret
    }
}
