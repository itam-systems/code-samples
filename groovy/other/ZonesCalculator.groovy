package anteeo.demeter.data
import anteeo.demeter.AgreementZonePriceList
import anteeo.demeter.TariffZone
import anteeo.demeter.energy.PriceListService
import anteeo.demeter.energy.TariffService
import anteeo.demeter.utils.DateUtils
import grails.util.Holders
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 4/25/16.
 * Class with functionality to calculate costs and usage separated in tariff zones
 * At this moment it's oriented only on Electrical Energy
 */
@CompileStatic
class ZonesCalculator {
    TariffService tariffService = (TariffService)Holders.grailsApplication.mainContext.getBean("tariffService")
    PriceListService priceListService = (PriceListService)Holders.grailsApplication.mainContext.getBean("priceListService")
    private List<List> tariffs = [] as List<List>
    private Date start
    private Date end
    private List<Map> points
    private List<List> zonesAndPricesInTimeline = [] as List<List>
    private List<Map> pointsZonesConsumption = [] as List<Map>

    /**
     * Constructor of class, taking points list as list of map, expected field in point map is "device"
     * Taking calculation time range start and calculation time range end
     * @param points
     * @param start
     * @param end
     */
    ZonesCalculator(List<Map> points, Date start, Date end){
        this.points = points
        this.start = start
        this.end = end
        for(Integer pointIndex = 0; pointIndex < points.size(); pointIndex++){
            Map point = points[pointIndex]
            if(point.containsKey("device") && point["device"] != null){
                List<Map> tariffsForPoint = tariffService.getTariffStructureForDeviceAndRange((Integer)point.device, start, end)
                List<AgreementZonePriceList> priceLists = priceListService.getByDeviceAndTimeRange((Integer)point.device, start, end)
                List pricesInTimeline = [] as List
                if(tariffsForPoint != null) {
                    List<Map> tariffsForDays = [] as List<Map>
                    Date currentTimeStep = start
                    Integer daysIndex = 0
                    for(Integer tariffIndex = 0; tariffIndex < tariffsForPoint.size(); tariffIndex++){
                        Map currentTariff = tariffsForPoint[tariffIndex]
                        if(currentTariff != null ) {
                            List<Long> currentZones = (List) currentTariff.zones
                            for (Integer priceListIndex = 0; priceListIndex < priceLists.size(); priceListIndex++) {
                                AgreementZonePriceList priceList = priceLists[priceListIndex]
                                while (currentTimeStep < end && currentTimeStep <= priceList.agreement.toDate) {
                                    Long zone = (Integer) currentZones[(Integer) currentTimeStep.toCalendar().get(Calendar.HOUR_OF_DAY)]
                                    TariffZone zoneObject = tariffService.getZone(zone)
                                    pricesInTimeline.push([endTime: DateUtils.addHours(currentTimeStep, 1).time, zoneId: zone, zoneSymbol: zoneObject.symbol, zoneColor: zoneObject.backgroundColor, price: priceList?.price + priceList?.variableElement])
                                    currentTimeStep = DateUtils.addHours(currentTimeStep, 1)
                                    if((daysIndex%24) == 0) {
                                        tariffsForDays.push(currentTariff)
                                        daysIndex++
                                    }
                                }

                            }
                        }
                    }
                    this.tariffs.push(tariffsForDays)
                }
                zonesAndPricesInTimeline.push(pricesInTimeline)

            }
        }
    }

    /**
     * Calculate consumption separated on zones basing on given data Set
     * Don't return calculation effect, saves it on instantion variable
     * @param data
     */
    public void calculateConsumptionInZones(List<MeteringPointValuesInRange> data){
        for(Integer pointIndex = 0; pointIndex < data.size();pointIndex++){
            MeteringPointValuesInRange pointData = data[pointIndex]
            List<Map> pointZones = zonesAndPricesInTimeline[pointIndex]
            Map zonesConsumption = [:] as Map
            Integer zoneIndex = 0
            for(Integer dataIndex = 0; dataIndex < pointData.data.size();dataIndex++){
                if(pointZones[zoneIndex] != null) {
                    if (pointData.data[dataIndex].time <= (Long) pointZones[zoneIndex].endTime) {
                        Map<String, BigDecimal> zone = (Map<String, BigDecimal>) zonesConsumption["${pointZones[zoneIndex].zoneId}"]
                        if (zone == null) {
                            zonesConsumption["${pointZones[zoneIndex].zoneId}"] = [consumption: (BigDecimal) 0, cost: (BigDecimal) 0] as Map
                            zone = (Map<String, BigDecimal>) zonesConsumption["${pointZones[zoneIndex].zoneId}"]
                        }
                        BigDecimal currVal = (pointData.data[dataIndex].val != null) ? (BigDecimal) (pointData.data[dataIndex].val) : (BigDecimal)0
                        zone.consumption = zone.consumption + currVal
                        zone.cost = zone.cost + (currVal * (BigDecimal) pointZones[zoneIndex].price)
                    } else {
                        zoneIndex++
                        dataIndex--
                    }
                }
            }
            this.pointsZonesConsumption.push(zonesConsumption)
        }
    }

    /**
     * Returns calculation effect from instance variable
     * Should be called after calculate method
     * @return
     */
    public List<Map> getConsumptionInZones(){
        return this.pointsZonesConsumption
    }

    /**
     * Returns zones and prices on timeline in hours interval
     * Can be called after instantion initialization, don't need calculate to be called
     * @return
     */
    public List<List> getZonesMarks(){
        return this.zonesAndPricesInTimeline
    }

    /**
     * Return tariffs information
     * Can be called after initialization, don't need calculation
     * @return
     */
    public List<List> getTariffs(){
        return tariffs
    }
}
