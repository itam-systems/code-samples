package anteeo.demeter.data

import anteeo.demeter.MeteringPoint
import anteeo.demeter.MeteringPointService
import anteeo.demeter.mpValues.ReducedMpValuesService
import anteeo.demeter.utils.Interval
import grails.util.Holders
import groovy.transform.CompileStatic
import org.apache.log4j.Logger

import static anteeo.demeter.ITags.MP_DIFF_D
import static anteeo.demeter.ITags.MP_DIFF_H
import static anteeo.demeter.ITags.MP_DIFF_QUARTER
import static anteeo.demeter.ITags.MP_COUNTER_TAG


/**
 * Created by Piotr Czubek on 3/16/16.
 */
@CompileStatic
class MeteringPointValuesInRange implements Serializable {
    public static final Integer CALC_COUNTER = 0
    public static final Integer CALC_DIFF_IN_RANGE = 1
    public static final Integer DATA_TYPE_POWER = 1
    public static final Integer DATA_TYPE_ENERGY = 0
    private MeteringPointService meteringPointService = (MeteringPointService)Holders.grailsApplication.mainContext.getBean('meteringPointService')
    private ReducedMpValuesService mpValuesService = (ReducedMpValuesService)Holders.grailsApplication.mainContext.getBean('reducedMpValuesService')
    private Date start
    private Date end
    private List<MeteringPoint> points = [] as List<MeteringPoint>
    private List<ReducedMeteringPointValue> values
    private Boolean readSuccess = false
    public static final Boolean ASC = true
    public static final Boolean DESC = false
    private Interval valuesInterval
    private Integer calculationAlgorithm
    private Integer dataType
    private BigDecimal multiplicationFactor = 1
    private Boolean manualCreated

    Logger log = Logger.getLogger(getClass())

    public MeteringPointValuesInRange(String nodeid, Date from, Date to, manualCreated = false, Interval valuesInterval = null){
        this.manualCreated = manualCreated
        points.push(meteringPointService.findByNodeid(nodeid))
        start = from
        end = to
        if(!manualCreated) {
            getCalculationAlgorithmFromPoint()
            this.valuesInterval = calculateValuesInterval(points)
            readSuccess = readDataFromDb()

        }
        else{
            values = [] as List<ReducedMeteringPointValue>
            readSuccess = true
            this.valuesInterval = valuesInterval
        }
    }

    public MeteringPointValuesInRange(List<String> nodeids, Date from, Date to, manualCreated = false, Interval valuesInterval = null){
        nodeids.each { String nodeid
            points.push(meteringPointService.findByNodeid(nodeid))
        }
        start = from
        end = to
        if(!manualCreated) {
            getCalculationAlgorithmFromPoint()
            this.valuesInterval = calculateValuesInterval(points)
            readSuccess = readDataFromDb()
        }
        else{
            values = [] as List<ReducedMeteringPointValue>
            readSuccess = true
            this.valuesInterval = valuesInterval
        }
    }

    public MeteringPoint getPoint(){
        return points[0]
    }

    public Interval getValuesInterval(){
        return valuesInterval
    }

    public void setValuesInterval(Interval valuesInterval){
        this.valuesInterval = valuesInterval
    }

    public List<MeteringPoint> getPoints(){
        return points
    }

    public String getNodeid(){
        String nodeid = ""
        points.each{ MeteringPoint point ->
            nodeid += point.nodeid
        }
        return nodeid
    }

    public Date getStart(){
        return start
    }
    public void setStart( Date start){
        this.start = start
    }

    public Date getEnd(){
        return end
    }

    public void setEnd( Date end){
        this.end = end
    }

    public List<ReducedMeteringPointValue> getData(){
        return values
    }

    public setData(List<ReducedMeteringPointValue> data){
        values = data
    }

    public Boolean readedSuccessfully(){
        return readSuccess
    }

    public void setCalculationAlgorithm(Integer calculationAlgorithm){
        this.calculationAlgorithm = calculationAlgorithm
    }

    public void setDataType(Integer dataType){
        this.dataType = dataType
    }

    public Integer getDataType(){
        return this.dataType
    }

    public Integer getCalculationAlgorithm(){
        return this.calculationAlgorithm
    }

    public Boolean getManualCreated(){
        return this.manualCreated
    }

    public Boolean isEmpty(){
        return (values.size() == 0)
    }
    public sortByValue(Boolean asc){
        this.values.sort{ ReducedMeteringPointValue a, ReducedMeteringPointValue b ->
            if(asc) {
                return a.val <=> b.val
            }
            else{
                return b.val <=> a.val
            }
        }
    }

    public sortByTime(Boolean asc){
        this.values.sort{ ReducedMeteringPointValue a, ReducedMeteringPointValue b ->
            if(asc) {
                return a.time <=> b.time
            }
            else{
                return b.time <=> a.time
            }
        }
    }

    public void roundToInterval(){
        this.values.collect{
            it.roundToInterval(this.valuesInterval)
        }
    }

    public List<ReducedMeteringPointValue>getValuesForTimeRange(Date from, Date to){
        return getValuesForTimeRange(from.time, to.time)
    }

    public List<ReducedMeteringPointValue>getValuesForTimeRange(Long from, Long to){
        return this.values.findAll{ ReducedMeteringPointValue it ->
            Boolean ret = (it.time >= from && it.time <= to)
            return ret
        }

    }

    public Integer size(){
        return values.size()
    }

    public push(ReducedMeteringPointValue val){
        values.push(val)
    }

    private Boolean readDataFromDb(){
        Boolean ret
        try {
                values = mpValuesService.getValues(point, start, end)

            ret = true
        }
        catch(Exception e){
            log.error e.message
            ret = false
        }
        return ret
    }

    public String toString(){
        return values.toString()
    }

    public Boolean isManualCreated(){
        return this.manualCreated
    }

    public void setManualCreated(Boolean manualCreated){
        this.manualCreated = manualCreated
    }

    public void countDiffs(){
        List<ReducedMeteringPointValue> tmpVals = [] as List<ReducedMeteringPointValue>
        BigDecimal prev
        for(Integer i = 0; i < this.values.size(); i++) {
            ReducedMeteringPointValue item = this.values[i]
                if (calculationAlgorithm != CALC_DIFF_IN_RANGE && item.val != null) {
                    if(i == 0){
                        prev = item.val

                    }
                    else if (prev != null) {
                        BigDecimal tmp = item.val - prev
                        prev = item.val
                        tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: tmp, status: item.getStatus()))

                    }
                    else {
                        prev = item.val
                        tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: (BigDecimal) null, status: ReducedMeteringPointValue.DATA_STATE.GAP.getValue()))

                    }
                } else {
                    tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: item.val, status: item.getStatus()))
                }
        }
        this.calculationAlgorithm = CALC_DIFF_IN_RANGE
        this.values = tmpVals
    }

    public void toPower(){
        this.multiplicationFactor = calculateMultiplicationFactorToPower()
        List<ReducedMeteringPointValue> tmpVals = [] as List<ReducedMeteringPointValue>
        for(Integer i = 0; i< this.values.size(); i++) {
            ReducedMeteringPointValue item = this.values[i]
            if (dataType != DATA_TYPE_POWER && item.val != null) {
                tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: item.val * multiplicationFactor, status: item.getStatus()))
            }
            else{
                tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: item.getVal(), status: item.getStatus()))
            }
        }
        this.dataType = DATA_TYPE_POWER
        this.values = tmpVals
    }


    public void toEnergy(){
        this.multiplicationFactor = calculateMultiplicationFactorToEnergy()
        List<ReducedMeteringPointValue> tmpVals = [] as List<ReducedMeteringPointValue>
        for(Integer i = 0; i< this.values.size(); i++) {
            ReducedMeteringPointValue item = this.values[i]
            if(dataType != DATA_TYPE_ENERGY && item.val != null ) {
                tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: item.val * multiplicationFactor, status: item.getStatus()))
            }
            else{
                tmpVals.push(new ReducedMeteringPointValue(pointId: item.getPointId(), time: item.getTime(), val: item.getVal(), status: item.getStatus()))
            }
        }
        this.dataType = DATA_TYPE_ENERGY
        this.values = tmpVals
    }

    private Interval calculateValuesInterval(List<MeteringPoint> points){
        List<Interval> intervals = [] as List<Interval>
        for(point in points) {
            if (meteringPointService.containsRequiredTag([MP_DIFF_QUARTER.toString()], meteringPointService.findByNodeid(point.nodeid).getTags())) {
                intervals.push(new Interval(Interval.QUARTER))
            } else if (meteringPointService.containsRequiredTag([MP_DIFF_H.toString()], meteringPointService.findByNodeid(point.nodeid).getTags())) {
                intervals.push(new Interval(Interval.HOUR))
            } else if (meteringPointService.containsRequiredTag([MP_DIFF_D.toString()], meteringPointService.findByNodeid(point.nodeid).getTags())) {
                intervals.push(new Interval(Interval.DAY))
            } else {
                intervals.push(new Interval(point))
            }
        }
        new Interval(Interval.getGcd(intervals))
    }

    private BigDecimal calculateMultiplicationFactorToEnergy(){
        if(manualCreated){
            return valuesInterval.calculateMultiplicationFactorToEnergy()
        }
        else {
            if (meteringPointService.containsRequiredTag([MP_DIFF_QUARTER.toString()], meteringPointService.findByNodeid(this.point.nodeid).getTags())) {
                return (BigDecimal) 0.25
            }
            if (meteringPointService.containsRequiredTag([MP_DIFF_H.toString()], meteringPointService.findByNodeid(this.point.nodeid).getTags())) {
                return (BigDecimal) 1
            }
            if (meteringPointService.containsRequiredTag([MP_DIFF_D.toString()], meteringPointService.findByNodeid(this.point.nodeid).getTags())) {
                return (BigDecimal) 24
            }
            return valuesInterval.calculateMultiplicationFactorToEnergy()
        }
    }


    private BigDecimal calculateMultiplicationFactorToPower(){
        if(manualCreated){
            return valuesInterval.calculateMultiplicationFactorToPower()
        }
        else {
            if (meteringPointService.containsRequiredTag([MP_COUNTER_TAG.toString()], meteringPointService.findByNodeid(this.point.nodeid).getTags())) {
                return (BigDecimal) 4
            }
            return valuesInterval.calculateMultiplicationFactorToPower()
        }
    }

    private getCalculationAlgorithmFromPoint(){
        if(meteringPointService.containsRequiredTag([MP_COUNTER_TAG.toString()], meteringPointService.findByNodeid(this.point.nodeid).getTags())){
            this.calculationAlgorithm = CALC_COUNTER
            this.dataType = DATA_TYPE_ENERGY
        }
        else{
            this.calculationAlgorithm = CALC_DIFF_IN_RANGE
            this.dataType = DATA_TYPE_POWER
        }
    }

    public MeteringPointValuesInRange clone(){
        MeteringPointValuesInRange ret = new MeteringPointValuesInRange(this.point.nodeid, this.start, this.end, true, this.valuesInterval)
        ret.setDataType(this.dataType)
        ret.setCalculationAlgorithm(this.calculationAlgorithm)
        ret.setData(this.data.collect{it})
        ret.multiplicationFactor = this.multiplicationFactor

        return ret
    }
}
