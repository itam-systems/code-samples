package anteeo.demeter.data

import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic
import anteeo.demeter.utils.Interval

/**
 * Created by user on 4/1/16.
 */
@CompileStatic
class CoherentTimeline  implements IReducedValuesManipulator{
    private Interval baseInterval
    private Boolean linear
    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>

    public CoherentTimeline(Boolean linear, Interval baseInterval = null){
        this.baseInterval = baseInterval
        this.linear = linear
    }

    public void calculate(List<MeteringPointValuesInRange> mpValues){
        if(!baseInterval){
            getBaseInterval(mpValues)
        }
        def stepsNumber = 1
        def lowestStart = mpValues[0].start.time
        def highestEnd = mpValues[0].end.time
        List<Integer> pointsFreq = []
        List<Integer> positions = []
        for(int pointIndex; pointIndex < mpValues.size();pointIndex++){
            MeteringPointValuesInRange entry = mpValues[pointIndex]
            MeteringPointValuesInRange tmpVals = new MeteringPointValuesInRange(entry.point.nodeid, entry.start, entry.end, true, baseInterval)
            tmpVals.setDataType(entry.dataType)
            tmpVals.setCalculationAlgorithm(entry.calculationAlgorithm)
            values.push(tmpVals)
            if (entry.start.time < lowestStart){
                lowestStart = entry.start.time
            }
            if(entry.end.time > highestEnd){
                highestEnd = entry.end.time
            }
            def tmpInterval = entry.getValuesInterval()
            pointsFreq.push((tmpInterval.toMilis()/baseInterval.toMilis()).toInteger())
            positions.push(0)
        }

        stepsNumber += (Integer)((highestEnd - lowestStart)/baseInterval.toMilis())
        for (int dataIndex =0; dataIndex < stepsNumber; dataIndex++){
            Long step = (Long)(lowestStart + dataIndex * baseInterval.toMilis())
            Boolean rowEmpty = true
            List<ReducedMeteringPointValue> tmpVals = [] as List<ReducedMeteringPointValue>
            for(int pointIndex; pointIndex < mpValues.size();pointIndex++){
                MeteringPointValuesInRange entry = mpValues[pointIndex]
                entry.roundToInterval()
                if((dataIndex%pointsFreq[pointIndex]) == 0){
                    def dataEntry = entry.data.getAt(positions[pointIndex])
                    if( dataEntry != null && (step - (baseInterval.toMilis()/2) < dataEntry.time) && (dataEntry.time < (step + (Long)(baseInterval.toMilis()/2)))){
                        tmpVals[pointIndex] = new ReducedMeteringPointValue(pointId: entry.point.id, time: step, val: (BigDecimal)dataEntry.val, status: ReducedMeteringPointValue.DATA_STATE.OK.getValue())
                        positions[pointIndex] += 1
                    }
                    else{
                        tmpVals[pointIndex] = new ReducedMeteringPointValue(pointId: entry.point.id, time: step, val: (BigDecimal)null, status: ReducedMeteringPointValue.DATA_STATE.GAP.getValue())
                    }
                    rowEmpty = false
                }
                else{
                    tmpVals[pointIndex] = new ReducedMeteringPointValue(pointId: entry.point.id, time: step, val: (BigDecimal)null, status: ReducedMeteringPointValue.DATA_STATE.NOT_EXPECTED.getValue())
                }
            }
            if(linear || !rowEmpty){
                for(int pointIndex; pointIndex < mpValues.size();pointIndex++){
                    values[pointIndex].push(tmpVals[pointIndex])
                }
            }
        }

    }

    public List<MeteringPointValuesInRange> getResult(){
        return values
    }

    private void getBaseInterval(List<MeteringPointValuesInRange> mpValues){
        List<Interval> intervals = [] as List<Interval>
        for(dataForMp in mpValues){
            intervals.push(new Interval(dataForMp.getPoint()))
        }
        baseInterval = new Interval(Interval.getGcd(intervals))
    }

}
