package anteeo.demeter.data

import anteeo.demeter.data.agregators.IAgregator
import anteeo.demeter.utils.Interval
import groovy.transform.CompileStatic
import org.apache.log4j.Logger

/**
 * Created by Piotr Czubek on 6/3/16.
 */
@CompileStatic
class IntervalAgregator implements IReducedValuesManipulator{
    Logger log = Logger.getLogger(getClass())

    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>
    Interval interval
    IAgregator agregator

    IntervalAgregator(Interval interval,  IAgregator agregator){
        this.interval = interval
        this.agregator = agregator
    }

    public void calculate(List<MeteringPointValuesInRange> mpValues){
        for(Integer pointIndex = 0; pointIndex < mpValues.size(); pointIndex++) {
            if(mpValues[pointIndex] != null) {
                MeteringPointValuesInRange pointEntries = mpValues[pointIndex]
                MeteringPointValuesInRange pointGeneratedVals = new MeteringPointValuesInRange(mpValues[pointIndex].point.nodeid, mpValues[pointIndex].start, mpValues[pointIndex].end, true, this.interval)
                pointGeneratedVals.setDataType(pointEntries.dataType)
                pointGeneratedVals.setCalculationAlgorithm(pointEntries.calculationAlgorithm)
                Integer intervalStartIndex = 0
                if(!pointEntries.data.isEmpty()) {
                    Long endTime
                    if(this.interval.toMilis() == pointEntries.valuesInterval.toMilis()){
                        endTime = (Long)mpValues[pointIndex].start.getTime()
                    }
                    else if(((Long)mpValues[pointIndex].start.getTime() + this.interval.toMilis() ) >= (Long)mpValues[pointIndex].end.getTime()){
                        endTime = (Long)mpValues[pointIndex].end.getTime()
                    }else{
                        (endTime = (Long)mpValues[pointIndex].start.getTime() + this.interval.toMilis())
                    }
                    for (Integer pointEntryIndex = 0; pointEntryIndex < pointEntries.data.size(); pointEntryIndex++) {
                        if (pointEntries.data[pointEntryIndex].time >= endTime  || pointEntryIndex == (pointEntries.data.size() -1)) {
                            List<ReducedMeteringPointValue> dataPart = pointEntries.data[intervalStartIndex..pointEntryIndex]
                            BigDecimal val = this.agregator.calculate(dataPart)
                            pointGeneratedVals.push(new ReducedMeteringPointValue(pointId: pointEntries.getPoint().id, time: this.agregator.getTime(pointEntries.data[intervalStartIndex].time, endTime), val: val, status: this.agregator.getStatus()))
                            intervalStartIndex = pointEntryIndex + 1
                            endTime = endTime + interval.toMilis()
                        }
                    }
                }
                values.push(pointGeneratedVals)
            }
            else{
                log.debug "Empty data entry on position ${pointIndex}"
            }
        }
    }

    public List<MeteringPointValuesInRange> getResult(){
        return values
    }



}
