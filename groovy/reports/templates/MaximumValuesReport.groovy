package anteeo.demeter.reports.templates

import anteeo.demeter.Report
import anteeo.demeter.reports.data.NMaxValues
import anteeo.demeter.reports.data.ValuesHigherThan
import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.reports.core.DemeterReport
import anteeo.demeter.reports.core.ReportParams
import anteeo.demeter.reports.data.ReportFormat
import anteeo.demeter.reports.design.DemeterMaximumValuesDesign
import ar.com.fdvs.dj.core.DynamicJasperHelper
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import groovy.json.JsonBuilder
import groovy.transform.CompileStatic
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource

import javax.servlet.http.HttpServletRequest

/**
 * Created by Piotr Czubek on 3/30/16.
 */
@CompileStatic
class MaximumValuesReport extends DemeterReport{

    def dataMap = [:]
    Map <String, Object> paramsMap = [:]

    public Map asMap(Report report) {
        Map result = baseMap(report)
        mpValuesService.completeMpsMap(result, result.mps, false)
        reportParams = new ReportParams(result)
        return result
    }

    Map create(Map params) {
        if((params.fromDate as long) >= (params.toDate as long)) {
            throw new IllegalArgumentException("bad dates")
        }
        def data = [:]
        data.name = params.name
        data.fromDate = params.fromDate
        data.toDate = params.toDate
        data.dateMode = params.dateMode
        data.optionsList = params.optionsList
        data.mpsForm = params.mps
        data.valueThresholds = [:]
        data.amountThresholds = [:]
        data.allValue = [:]
        data.allAmount = [:]
        List<String> mps = [] as List<String>
        Map<String,Map> options =  (Map<String,Map>)params.optionsList
        for(it in options) {
            mps.push(it.key)
            data.valueThresholds[(String)it.key] = it.value.valueThreshold
            data.amountThresholds[(String)it.key] = it.value.amountThreshold
            data.allValue[(String)it.key] = it.value.allValue
            data.allAmount[(String)it.key] = it.value.allAmount
        }
        data.mps = mps
        [type: 3, data:  new JsonBuilder(data).toString()]
    }

    public DynamicReportBuilder buildReport(Map<String, Object> report, HttpServletRequest request) {
        Locale locale = request.getLocale()
        reportParams.setLocale(locale)
        def reportObject = new DemeterMaximumValuesDesign(reportParams)
        def builder = reportObject.createBuilder()
        createDataSource(report)
        createHeader(reportObject.getLeftHeaderData(), reportObject.getRightHeaderData())
        return builder
    }

    public List<JasperPrint> getJasperPrintList(Map report, HttpServletRequest request){
        Locale locale = request.getLocale()
        DynamicReportBuilder compiledReport = buildReport(report, request)
        List<JasperPrint> printList = [] as List<JasperPrint>
        printList.push(DynamicJasperHelper.generateJasperPrint(compiledReport.build(), new ClassicLayoutManager(), getDataSource(report, locale), getParams()))
        return printList
    }

    public void createDataSource(Map<String, Object> report){
        List<MeteringPointValuesInRange> rawValues = [] as List<MeteringPointValuesInRange>
        List<String> points = (List<String>)report.mps
        List<BigDecimal> valueLimits =  [] as List<BigDecimal>
        List<Integer> valueCounts = [] as List<Integer>
        for (point in points){
            rawValues.push(new MeteringPointValuesInRange(point, new Date((Long)report.fromDate), new Date((Long)report.toDate)))
        }
        Map valueThresholds = (Map)report.valueThresholds
        Map allValue = (Map)report.allValue
        Map amountThresholds = (Map)report.amountThresholds
        Map allAmount = (Map)report.allAmount
        for(point in points){
            if(allValue[point]){
                valueLimits.push(null)
            }
            else {
                valueLimits.push(new BigDecimal(valueThresholds[point].toString()))
            }
            if(allAmount[point]){
                valueCounts.push(null)
            }
            else {
                valueCounts.push(new Integer(amountThresholds[point].toString()))
            }
        }

        IReducedValuesManipulator countLimited = new NMaxValues(valueCounts)
        countLimited.calculate(rawValues)
        IReducedValuesManipulator valueLimited = new ValuesHigherThan(valueLimits)
        valueLimited.calculate(countLimited.getResult())

        List<Map> reportData = ReportFormat.simpleFormatter(valueLimited.getResult())
        this.paramsMap.put("tableData",reportData)
    }

    def createHeader(List leftArr, List rightArr){
        def cnt = (leftArr.size() > rightArr.size()) ? leftArr.size() : rightArr.size()
        def result = []
        for (int i=0;i < cnt;i++){
            result.push([leftHeader: (String)leftArr[i]?:"", rightHeader: (String)rightArr[i]?:""])
        }
        this.paramsMap.put("header",result)
    }


    Map getParams(){
        return this.paramsMap
    }

    JRDataSource getDataSource(Map<String,Object> report, Locale locale) {
        return new JRBeanCollectionDataSource([this.dataMap])
    }
}
