package anteeo.demeter.reports.templates

import anteeo.demeter.MeteringPointService
import anteeo.demeter.Report
import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.IntervalAgregator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.data.agregators.DiffAgregator
import anteeo.demeter.data.agregators.IAgregator
import anteeo.demeter.data.agregators.SumAgregator
import anteeo.demeter.reports.core.DemeterReport
import anteeo.demeter.reports.core.ReportParams
import anteeo.demeter.reports.data.ReportFormat
import anteeo.demeter.data.ZonesCalculator
import anteeo.demeter.reports.design.DemeterConsumptionDesign
import anteeo.demeter.reports.helpers.common.ScheduleService
import anteeo.demeter.reports.helpers.common.TariffNamesService
import anteeo.demeter.reports.helpers.groups.GroupService
import anteeo.demeter.reports.helpers.nodes.NodeEnergyService
import anteeo.demeter.utils.Interval
import ar.com.fdvs.dj.core.DynamicJasperHelper
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import grails.util.Holders
import groovy.json.JsonBuilder
import groovy.transform.CompileStatic
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import org.apache.commons.lang.time.DateUtils

import javax.servlet.http.HttpServletRequest

import static anteeo.demeter.ITags.MP_DIFF_TAG
import static anteeo.demeter.ITags.MP_COUNTER_TAG

/**
 * Created by Piotr Czubek on 3/31/16.
 */
@CompileStatic
class ConsumptionReport extends DemeterReport{

    def dataMap = [:]
    Map<String, Object> paramsMap = [:] as Map<String, Object>

    GroupService groupService = (GroupService)Holders.grailsApplication.mainContext.getBean("groupService")
    NodeEnergyService nodeEnergyService = (NodeEnergyService)Holders.grailsApplication.mainContext.getBean("nodeEnergyService")
    ScheduleService scheduleService = (ScheduleService)Holders.grailsApplication.mainContext.getBean("scheduleService")
    TariffNamesService tariffNamesService = (TariffNamesService)Holders.grailsApplication.mainContext.getBean("tariffNamesService")
    MeteringPointService meteringPointService = (MeteringPointService)Holders.grailsApplication.mainContext.getBean("meteringPointService")


    private def updateTreeAndStructure(Map report) {
        report.tree = groupService.getTreeWithNewGroups(report.tree)
        List<Map> mps = (List<Map>)nodeEnergyService.getMpsFromTree(report.tree)
        List tmpMps = [] as List
        List tmpSchedules = [] as List
        List tmpTariffs = [] as List
        for(it in mps) {
            tmpMps.push(it.nodeid)
            tmpSchedules.push(it.chosenSchedule)
            tmpTariffs.push(it.chosenTariff)
        }
        report.mps = tmpMps
        report.schedules = tmpSchedules
        report.tariffs = tmpTariffs
        report.structure = nodeEnergyService.getUpdatedStructure(report.tree, getTags())
    }

    def getTags() {
        [MP_COUNTER_TAG, MP_DIFF_TAG]
    }

    Map asMap(Report report) {
        def result = baseMap(report)
        updateTreeAndStructure(result)
        mpValuesService.completeMpsMap(result, result.mps, false)
        scheduleService.completeSchedulesMap(result, result.schedules)
        tariffNamesService.completeTariffsMap(result, result.tariffs)
        reportParams = new ReportParams(result)
        return result
    }

    Map create(Map params) {
        if((params.fromDate as long) > (params.toDate as long)) {
            throw new IllegalArgumentException("bad dates")
        }
        def data = [:]
        data.name = params.name
        data.fromDate = params.fromDate
        data.toDate = params.toDate
        data.dateMode = params.dateMode
        data.tree = nodeEnergyService.generateNodesFromTree(params.structure)
        return[type: 5, data: new JsonBuilder(data).toString()]
    }

    public DynamicReportBuilder buildReport(Map<String, Object> report, HttpServletRequest request) {
        Locale locale = request.getLocale()
        reportParams.setLocale(locale)
        def reportObject = new DemeterConsumptionDesign(reportParams)
        def builder = reportObject.createBuilder()
        createDataSource(report)
        createHeader(reportObject.getLeftHeaderData(), reportObject.getRightHeaderData())
        return builder
    }

    public List<JasperPrint> getJasperPrintList(Map report, HttpServletRequest request){
        Locale locale = request.getLocale()
        DynamicReportBuilder compiledReport = buildReport(report, request)
        List<JasperPrint> printList = [] as List<JasperPrint>
        printList.push(DynamicJasperHelper.generateJasperPrint(compiledReport.build(), new ClassicLayoutManager(), getDataSource(report, locale), getParams()))
        return printList
    }

    public void createDataSource(Map<String, Object> report){
        reportParams.groupsStructure.each{ Map group ->
            Map<Integer, List> media = (Map<Integer, List<Map>>)group.media
            media.each{ Integer mediaId, List<Map> points ->
                List<MeteringPointValuesInRange> rawValues = [] as List<MeteringPointValuesInRange>
                ZonesCalculator zonesCalculator = new ZonesCalculator(points, reportParams.getStartDate(), reportParams.getEndDate())
                IAgregator agregator
                for(Integer pointIndex = 0; pointIndex < points.size(); pointIndex++){
                    Map point = points[pointIndex]
                    MeteringPointValuesInRange data
                    if(pointIsDiff((String)point.nodeid)) {
                        data = new MeteringPointValuesInRange((String)point.nodeid, DateUtils.addMinutes(new Date((Long)report.fromDate), 15), new Date((Long)report.toDate))
                        data.sortByTime(MeteringPointValuesInRange.ASC)
                        data.setCalculationAlgorithm(MeteringPointValuesInRange.CALC_DIFF_IN_RANGE)
                        points[pointIndex]["unit"] = reportParams.mediaUtils.switchUnit(mediaId, (String)point["unit"])
                        data.toEnergy()
                        agregator = new SumAgregator()
                    }
                    else {
                        data = new MeteringPointValuesInRange((String)point.nodeid, new Date((Long)report.fromDate), new Date((Long)report.toDate))
                        data.sortByTime(MeteringPointValuesInRange.ASC)
                        data.setCalculationAlgorithm(MeteringPointValuesInRange.CALC_COUNTER)
                        data.countDiffs()
                        agregator = new DiffAgregator()
                    }
                    rawValues.push(data)
                }
                Long toDate = (Long)report.toDate
                Long fromDate = (Long)report.fromDate
                IReducedValuesManipulator calculatedValues = new IntervalAgregator(new Interval(toDate - fromDate), agregator)
                calculatedValues.calculate(rawValues)
                zonesCalculator.calculateConsumptionInZones(rawValues)
                List<Map> zonesConsumptionSummary = zonesCalculator.getConsumptionInZones()
                def reportData = ReportFormat.consumptionFormatter(calculatedValues.getResult(), points, zonesConsumptionSummary)
                this.paramsMap.put((String)"tableData${group.id}${reportParams.mediaUtils.getMediaName(mediaId)}", reportData)
            }
        }

    }

    def createHeader(List leftArr, List rightArr){
        def cnt = (leftArr.size() > rightArr.size()) ? leftArr.size() : rightArr.size()
        def result = []
        for (int i=0;i<cnt;i++){
            result.push([leftHeader: (String)leftArr[i]?:"", rightHeader: (String)rightArr[i]?:""])
        }
        this.paramsMap.put("header",result)
    }

    Map getParams(){
        return this.paramsMap
    }
    JRDataSource getDataSource(Map<String,Object> report, Locale locale) {
        return new JRBeanCollectionDataSource([this.dataMap])
    }
}
