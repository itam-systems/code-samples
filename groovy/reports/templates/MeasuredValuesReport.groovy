package anteeo.demeter.reports.templates

import anteeo.demeter.Report
import anteeo.demeter.data.CoherentTimeline
import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.reports.core.DemeterReport
import anteeo.demeter.reports.core.ReportParams
import anteeo.demeter.reports.data.ReportFormat
import anteeo.demeter.reports.design.DemeterMeasuredValuesDesign
import ar.com.fdvs.dj.core.DynamicJasperHelper
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import groovy.json.JsonBuilder
import groovy.transform.CompileStatic
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource

import javax.servlet.http.HttpServletRequest

/**
 * Created by Piotr Czubek on 3/31/16.
 */
@CompileStatic
class MeasuredValuesReport extends DemeterReport{

    def dataMap = [:]
    Map<String, Object> paramsMap = [:] as Map<String, Object>

    public Map asMap(Report report) {
        Map result = baseMap(report)
        mpValuesService.completeMpsMap(result, result.mps, false)
        reportParams = new ReportParams(result)
        return result
    }

    public Map create(Map params) {
        if((params.fromDate as long) >= (params.toDate as long)) {
            return null
        }
        Map data = [:] as Map
        data.name = params.name
        data.fromDate = params.fromDate
        data.toDate = params.toDate
        data.dateMode = params.dateMode
        List mpsArr = [] as List
        data.mpsForm = params.mps

        List<Map<String, Object>> mps = (List<Map<String, Object>>)params.mps
        for(it in mps) {
            mpsArr.push(it["nodeid"].toString())
        }
        data.mps = mpsArr
        [type: 1, data: new JsonBuilder(data).toString()]
    }

    public DynamicReportBuilder buildReport(Map<String, Object> report, HttpServletRequest request) {
        Locale locale = request.getLocale()
        reportParams.setLocale(locale)

        def reportObject = new DemeterMeasuredValuesDesign(reportParams)
        def builder = reportObject.createBuilder()
        createDataSource(report)
        createHeader(reportObject.getLeftHeaderData(), reportObject.getRightHeaderData())
        return builder
    }

    public List<JasperPrint> getJasperPrintList(Map report, HttpServletRequest request){
        Locale locale = request.getLocale()
        DynamicReportBuilder compiledReport = buildReport(report, request)
        List<JasperPrint> printList = [] as List<JasperPrint>
        printList.push(DynamicJasperHelper.generateJasperPrint(compiledReport.build(), new ClassicLayoutManager(), getDataSource(report, locale), getParams()))
        return printList
    }

    public void createDataSource(Map<String, Object> report){
        List<MeteringPointValuesInRange> rawValues = [] as List<MeteringPointValuesInRange>
        List<String> points = (List<String>)report.mps
        for (point in points){
            rawValues.push(new MeteringPointValuesInRange(point, new Date((Long)report.fromDate), new Date((Long)report.toDate), false))
        }

        IReducedValuesManipulator synchronisedTimeline = new CoherentTimeline(false)
        synchronisedTimeline.calculate(rawValues)

        List<Map> reportData = ReportFormat.simpleFormatter(synchronisedTimeline.getResult())
        this.paramsMap.put("tableData", reportData)
    }

    def createHeader(List leftArr, List rightArr){
        def cnt = (leftArr.size() > rightArr.size()) ? leftArr.size() : rightArr.size()
        def result = []
        for (int i=0;i<cnt;i++){
            result.push([leftHeader: (String)leftArr[i]?:"", rightHeader: (String)rightArr[i]?:""])
        }
        this.paramsMap.put("header",result)
    }

    Map getParams(){
        return this.paramsMap
    }
    JRDataSource getDataSource(Map<String,Object> report, Locale locale) {
        return new JRBeanCollectionDataSource([this.dataMap])
    }
}
