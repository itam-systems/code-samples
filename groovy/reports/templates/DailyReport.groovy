package anteeo.demeter.reports.templates

import anteeo.demeter.MeteringPoint
import anteeo.demeter.MeteringPointService
import anteeo.demeter.Report
import anteeo.demeter.data.IntervalAgregator
import anteeo.demeter.data.agregators.Agregator
import anteeo.demeter.data.agregators.DiffAgregator
import anteeo.demeter.data.agregators.IAgregator
import anteeo.demeter.data.agregators.MaxAgregator
import anteeo.demeter.data.agregators.SumAgregator
import anteeo.demeter.reports.core.DemeterReport
import anteeo.demeter.reports.core.ReportParams
import anteeo.demeter.data.CoherentTimeline
import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.reports.data.ReportFormat
import anteeo.demeter.data.ToDiffs
import anteeo.demeter.data.ToEnergy
import anteeo.demeter.data.ToPower
import anteeo.demeter.data.ZonesCalculator
import anteeo.demeter.reports.design.DemeterDailyDesign
import anteeo.demeter.reports.design.DemeterReportFrameDesign
import anteeo.demeter.reports.helpers.common.ScheduleService
import anteeo.demeter.reports.helpers.common.TariffNamesService
import anteeo.demeter.reports.helpers.groups.GroupService
import anteeo.demeter.reports.helpers.nodes.NodeEnergyService
import anteeo.demeter.utils.Interval
import ar.com.fdvs.dj.core.DynamicJasperHelper
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import grails.util.Holders
import groovy.json.JsonBuilder
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JRStyle
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import org.apache.commons.lang.time.DateUtils

import javax.imageio.ImageIO
import javax.servlet.http.HttpServletRequest
import java.awt.image.BufferedImage

import static anteeo.demeter.ITags.MP_DIFF_TAG
import static anteeo.demeter.ITags.MP_COUNTER_TAG

/**
 * Created by Piotr Czubek on 4/11/16.
 */
class DailyReport extends DemeterReport{

    List dataMap = []
    Map<String, Object> paramsMap = [:] as Map<String, Object>
    ReportFormat formatter


    GroupService groupService = (GroupService)Holders.grailsApplication.mainContext.getBean("groupService")
    NodeEnergyService nodeEnergyService = (NodeEnergyService)Holders.grailsApplication.mainContext.getBean("nodeEnergyService")
    ScheduleService scheduleService = (ScheduleService)Holders.grailsApplication.mainContext.getBean("scheduleService")
    TariffNamesService tariffNamesService = (TariffNamesService)Holders.grailsApplication.mainContext.getBean("tariffNamesService")
    MeteringPointService meteringPointService = (MeteringPointService)Holders.grailsApplication.mainContext.getBean("meteringPointService")


    private def updateTreeAndStructure(Map report) {
        report.tree = groupService.getTreeWithNewGroups(report.tree)
        List<Map> mps = (List<Map>)nodeEnergyService.getMpsFromTree(report.tree)
        List tmpMps = [] as List
        List tmpSchedules = [] as List
        List tmpTariffs = [] as List
        for(it in mps) {
            tmpMps.push(it.nodeid)
            tmpSchedules.push(it.chosenSchedule)
            tmpTariffs.push(it.chosenTariff)
        }
        report.mps = tmpMps
        report.schedules = tmpSchedules
        report.tariffs = tmpTariffs
        report.structure = nodeEnergyService.getUpdatedStructure(report.tree, getTags())
    }

    def getTags() {
        [MP_COUNTER_TAG, MP_DIFF_TAG]
    }

    Map asMap(Report report) {
        def result = baseMap(report)
        updateTreeAndStructure(result)
        mpValuesService.completeMpsMap(result, result.mps, false)
        scheduleService.completeSchedulesMap(result, result.schedules)
        tariffNamesService.completeTariffsMap(result, result.tariffs)
        reportParams = new ReportParams(result)
        return result
    }

    Map create(Map params) {
        if((params.fromDate as long) > (params.toDate as long)) {
            throw new IllegalArgumentException("bad dates")
        }
        def data = [:]
        data.name = params.name
        data.fromDate = params.fromDate
        data.toDate = params.toDate
        data.dateMode = params.dateMode
        data.tree = nodeEnergyService.generateNodesFromTree(params.structure)
        return[type: 11, data: new JsonBuilder(data).toString()]
    }

    public DynamicReportBuilder buildReport(Map<String, Object> report, HttpServletRequest request) {
        Locale locale = request.getLocale()
        reportParams.setLocale(locale)
        formatter = new ReportFormat(locale)
        def reportObject = new DemeterDailyDesign(reportParams)
        def builder = reportObject.createBuilder()
        builder.setReportLocale(locale)
        createDataSource(report)
        createHeader(reportObject.getLeftHeaderData(), reportObject.getRightHeaderData())
        return builder
    }

    public List<JasperPrint> getJasperPrintList(Map report, HttpServletRequest request){
        Locale locale = request.getLocale()
        DynamicReportBuilder compiledReport = buildReport(report, request)
        List<JasperPrint> printList = [] as List<JasperPrint>
        compileIfNeccessary("sub_dzienny_strefy")
        compileIfNeccessary("sub_dzienny_cykle")
        compileIfNeccessary("dzienny")
        JasperPrint dynamicReport = DynamicJasperHelper.generateJasperPrint(compiledReport.build(), new ClassicLayoutManager(), getDataSource(report, locale), getParams())
        JasperPrint staticReport = JasperFillManager.fillReport((String)"${DemeterReportFrameDesign.templatesPath}/dzienny.jasper", getParams(), getDataSource(report, locale))
        JRStyle[] styleList = staticReport.getStyles();
        //TODO: Należy albo w tym miejscu próbować nadpisywać pobraną powyżej listę styli, albo przejść z raportem dziennym na DynamicJasper
        printList.push(dynamicReport)
        printList.push(staticReport)
        return printList
    }

    public void createDataSource(Map<String, Object> report){
        BufferedImage logo = ImageIO.read(new File(Holders.applicationContext.getResource("${Holders.grailsApplication.config.systemInfo.systemLogo}.jpg").getFile().getPath()))
        paramsMap.put("date_from", DemeterDailyDesign.dateFormatter.format(reportParams.getStartDate()))
        paramsMap.put("date_to", DemeterDailyDesign.dateFormatter.format(reportParams.getEndDate()))
        paramsMap.put("author", reportParams.getAuthor())
        paramsMap.put("report_name", reportParams.getTitle())
        paramsMap.put("logo", ReportFormat.encodeImageToString(logo, "jpg"))
        reportParams.groupsStructure.each{ Map group ->
            Map<Integer, List> media = (Map<Integer, List<Map>>)group.media
            media.each{ Integer mediaId, List<Map> points ->
                List<MeteringPointValuesInRange> rawValues = [] as List<MeteringPointValuesInRange>
                ZonesCalculator zonesCalculator = new ZonesCalculator(points, reportParams.getStartDate(), reportParams.getEndDate())
                IAgregator agregator
                points.eachWithIndex{Map point, int pointIndex ->
                    MeteringPointValuesInRange data
                    if(pointIsDiff((String)point.nodeid)) {
                        data = new MeteringPointValuesInRange((String)point.nodeid, DateUtils.addMinutes(new Date((Long)report.fromDate), 15), new Date((Long)report.toDate))
                        data.sortByTime(MeteringPointValuesInRange.ASC)
                        data.setCalculationAlgorithm(MeteringPointValuesInRange.CALC_DIFF_IN_RANGE)
                        points[pointIndex]["unit"] = reportParams.mediaUtils.switchUnit(mediaId, (String)point["unit"])
                        data.toEnergy()
                        agregator = new SumAgregator()
                    }
                    else {
                        data = new MeteringPointValuesInRange((String)point.nodeid, new Date((Long)report.fromDate), new Date((Long)report.toDate))
                        data.sortByTime(MeteringPointValuesInRange.ASC)
                        data.setCalculationAlgorithm(MeteringPointValuesInRange.CALC_COUNTER)
                        data.countDiffs()
                        agregator = new DiffAgregator()
                    }
                    rawValues.push(data)
                    for (Date date = new Date((Long)report.fromDate); date.before(new Date((Long)report.toDate)); date = DateUtils.addDays(date, 1)){
                        createDailyDataSet(report, point, date, mediaId)
                    }
                }
                IReducedValuesManipulator calculatedValues = new IntervalAgregator(new Interval((Long)report.toDate - (Long)report.fromDate), agregator)
                calculatedValues.calculate(rawValues)
                zonesCalculator.calculateConsumptionInZones(rawValues)
                List<Map> zonesConsumptionSummary = zonesCalculator.getConsumptionInZones()
                def reportData = ReportFormat.consumptionFormatter(calculatedValues.getResult(), points, zonesConsumptionSummary)
                this.paramsMap.put((String)"tableData${group.id}${reportParams.mediaUtils.getMediaName(mediaId)}", reportData)
            }
        }
    }

    def createDailyDataSet(Map<String, Object> report, Map point, Date date, Integer mediaId){
        List<MeteringPointValuesInRange> dayData
        Date baseEnd = (DateUtils.addDays(date, 1) > new Date((Long) report.toDate)) ? new Date((Long) report.toDate) :  DateUtils.addDays(date, 1)
        ZonesCalculator dayZonesCalculator = new ZonesCalculator([point], date, baseEnd)
        MeteringPoint mp = meteringPointService.findByNodeid((String)point.nodeid)
        if(meteringPointService.containsRequiredTag([MP_DIFF_TAG.toString()], mp.getTags())) {
            dayData = [new MeteringPointValuesInRange((String) point.nodeid, DateUtils.addMinutes(date, 15), DateUtils.addMinutes(baseEnd, 1))]
        }
        else{

            dayData = [new MeteringPointValuesInRange((String) point.nodeid, date, DateUtils.addMinutes(baseEnd, 1))]
        }

        //Round data to timeline and fill gaps with nulls
        IReducedValuesManipulator gapFiller = new CoherentTimeline(true)
        gapFiller.calculate(dayData)
        //Change counters to diffs
        IReducedValuesManipulator toDiffs = new ToDiffs()
        toDiffs.calculate(gapFiller.getResult())

        //Change data to power
        IReducedValuesManipulator asPower = new ToPower()
        asPower.calculate(toDiffs.getResult())

        //Count power in quarters
        IAgregator sumAgregator = new SumAgregator()
        if(meteringPointService.containsRequiredTag([MP_COUNTER_TAG.toString()], mp.getTags())) {
            asPower.getResult()[0].setStart(DateUtils.addMinutes(date, 15))
        }
        IReducedValuesManipulator powerInQuarters = new IntervalAgregator(new Interval(Interval.QUARTER), sumAgregator)
        powerInQuarters.calculate(asPower.getResult())

        //Count Energy in Hours
        IReducedValuesManipulator energyInQuarters = new ToEnergy()
        energyInQuarters.calculate(powerInQuarters.getResult())
        energyInQuarters.getResult()[0].setStart(date)
        powerInQuarters.getResult()[0].setStart(date)

        energyInQuarters.getResult()[0].setEnd(baseEnd)
        IReducedValuesManipulator energyInHours = new IntervalAgregator(new Interval(Interval.HOUR), sumAgregator)
        energyInHours.calculate(energyInQuarters.getResult())
        //Count day energy
        IAgregator daySumAgregator = new SumAgregator()
        daySumAgregator.setTimeMethod(Agregator.TIME_CALCULATION.AVG.value)
        IReducedValuesManipulator day = new IntervalAgregator(new Interval(Interval.DAY), daySumAgregator)
        day.calculate(energyInHours.getResult())
        //Find max value for day
        IAgregator maxAgregator = new MaxAgregator()
        IReducedValuesManipulator maxQuarter = new IntervalAgregator(new Interval(Interval.DAY), maxAgregator)
        maxQuarter.calculate(powerInQuarters.getResult())
        dayZonesCalculator.calculateConsumptionInZones(energyInHours.getResult())
        List<Map> zoneSums = dayZonesCalculator.getConsumptionInZones()
        List<List<Map>> zonesMarks = dayZonesCalculator.getZonesMarks()

        formatter.prepareDaily(
                day.getResult(),
                energyInHours.getResult(),
                powerInQuarters.getResult(),
                maxQuarter.getResult(),
                dayZonesCalculator.getTariffs(),
                zonesMarks,
                zoneSums,
                mediaId
        )
        def dailyData = formatter.dailyFormat()
        this.dataMap.addAll(dailyData)
    }

    def createHeader(List leftArr, List rightArr){
        def cnt = (leftArr.size() > rightArr.size()) ? leftArr.size() : rightArr.size()
        def result = []
        for (int i=0;i<cnt;i++){
            result.push([leftHeader: (String)leftArr[i]?:"", rightHeader: (String)rightArr[i]?:""])
        }
        this.paramsMap.put("header",result)
    }

    Map getParams(){
        return this.paramsMap
    }

    JRDataSource getDataSource(Map<String,Object> report, Locale locale) {
        return new JRBeanCollectionDataSource(this.dataMap)
    }

    void compileIfNeccessary(String reportName){
        File reportDefinition = new File((String)"${DemeterReportFrameDesign.templatesPath}/${reportName}.jrxml")
        File compiledReport = new File((String)"${DemeterReportFrameDesign.templatesPath}/${reportName}.jasper")
        if(reportDefinition.lastModified() > compiledReport.lastModified()){
            JasperCompileManager.compileReportToFile(reportDefinition.absolutePath)
        }
    }
}
