package anteeo.demeter.reports.design

import anteeo.demeter.reports.core.ReportParams
import ar.com.fdvs.dj.core.DJConstants
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.builders.ColumnBuilder
import ar.com.fdvs.dj.domain.constants.Border
import ar.com.fdvs.dj.domain.constants.HorizontalAlign
import ar.com.fdvs.dj.domain.constants.VerticalAlign
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn
import groovy.transform.CompileStatic
import grails.util.Holders
import org.springframework.test.context.web.WebAppConfiguration

import java.awt.Color
import java.text.SimpleDateFormat

import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import ar.com.fdvs.dj.domain.AutoText
import ar.com.fdvs.dj.domain.Style
import ar.com.fdvs.dj.domain.builders.StyleBuilder
import ar.com.fdvs.dj.core.layout.HorizontalBandAlignment
import ar.com.fdvs.dj.domain.constants.Font

/**
 * Created by Piotr Czubek on 2/18/16.
 */
@WebAppConfiguration
@CompileStatic
public abstract class DemeterReportFrameDesign {

    ReportParams reportParams

    protected Font subtitleFont
    protected Font contentFont
    protected Style contentStyle
    protected Style subtitleStyle
    protected Border defaultBorder
    protected Font headerFont
    protected Style headerFontStyle
    protected Font footerFont
    protected Style footerFontStyle
    protected Font titleFont
    protected Style titleStyle
    protected Font tableHeaderFont
    protected Style tableHeaderStyle
    protected Font tableContentFont
    protected Style tableContentStyle
    protected Style oddRowsStyle
    protected Style timelineContentStyle

    public static final SimpleDateFormat dateTimeFormatter =  new SimpleDateFormat("dd-MM-yyyy HH:mm")
    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy")
    public static final String templatesPath = Holders.applicationContext.getResource("/reports/templates/").getFile().getAbsolutePath()
    public static final String chartsPath = Holders.applicationContext.getResource("/reports/charts/").getFile().getPath()
    public DynamicReportBuilder baseBuilder

    /**
     * Universal report layout class creates all universal styles for reports and force consistent layout for all report classes
     * @param reportParams
     */
    public DemeterReportFrameDesign(ReportParams reportParams){
        this.reportParams = reportParams
        this.baseBuilder = new DynamicReportBuilder()
        this.buildStyles()
    }

    /**
     * Procedure to initialise all base styles for reports layout
     */
    private void buildStyles(){
        this.headerFont = new Font(12, Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.headerFontStyle = new StyleBuilder(false).setFont(headerFont).build()
        this.footerFont = new Font(12, Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.footerFontStyle = new StyleBuilder(false).setFont(footerFont).build()
        this.titleFont = new Font(18, Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.titleStyle =  new StyleBuilder(false).setFont(titleFont).setHorizontalAlign(HorizontalAlign.CENTER).build()
        this.contentFont = new Font(12, Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.subtitleFont = new Font(12, Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.subtitleFont.setBold(true)
        this.contentStyle = new StyleBuilder(false).setFont(this.contentFont).build()
        this.subtitleStyle = new StyleBuilder(false).setFont(this.subtitleFont).build()
        this.defaultBorder = new Border(1)
        this.defaultBorder.setColor(Color.BLACK)
        this.tableHeaderFont = new ar.com.fdvs.dj.domain.constants.Font(10, ar.com.fdvs.dj.domain.constants.Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), ar.com.fdvs.dj.domain.constants.Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.tableHeaderFont.setBold(true)
        this.tableHeaderStyle = new StyleBuilder(false)
                .setFont(tableHeaderFont)
                .setHorizontalAlign(HorizontalAlign.CENTER)
                .setVerticalAlign(VerticalAlign.MIDDLE)
                .setBackgroundColor(Color.GRAY)
                .setTransparent(false)
                .setBorder(this.defaultBorder)
                .build()
        this.tableContentFont = new ar.com.fdvs.dj.domain.constants.Font(10, ar.com.fdvs.dj.domain.constants.Font._FONT_ARIAL, Holders.applicationContext.getResource("/fonts/ARIALUNI.TTF").getFile().getAbsolutePath(), ar.com.fdvs.dj.domain.constants.Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true)
        this.tableContentStyle = new StyleBuilder(false)
                .setFont(tableContentFont)
                .setHorizontalAlign(HorizontalAlign.RIGHT)
                .setVerticalAlign(VerticalAlign.MIDDLE)
                .setBorder(this.defaultBorder)
                .setBorderColor(Color.BLACK)
                .build()
        this.timelineContentStyle = new StyleBuilder(false)
                .setFont(tableContentFont)
                .setHorizontalAlign(HorizontalAlign.CENTER)
                .setVerticalAlign(VerticalAlign.MIDDLE)
                .setBorder(this.defaultBorder)
                .setBorderColor(Color.BLACK)
                .build()
        this.oddRowsStyle = new StyleBuilder(false)
                .setBackgroundColor(Color.LIGHT_GRAY)
                .build()
        this.buildSpecyficStyles()
    }
    /**
     * Creates left header container as column
     * @return leftHeaderColumn
     */
    public AbstractColumn createLeftHeaderColumn(){
        def left = ColumnBuilder.getNew()
        left.setTitle(this.getLeftHeaderTitle())
        left.setStyle(contentStyle)
        left.setHeaderStyle(subtitleStyle)
        left.setColumnProperty("leftHeader",String.class.getName())
        return left.build()
    }
    /**
     * Creates right header container as column
     * @return rightHeaderColumn
     */
    public AbstractColumn createRightHeaderColumn(){
        def right = ColumnBuilder.getNew()
        right.setTitle(this.getRightHeaderTitle())
        right.setStyle(contentStyle)
        right.setHeaderStyle(subtitleStyle)
        right.setColumnProperty("rightHeader",String.class.getName())
        return right.build()
    }

    /**
     * Creates base DynamicReportBuilder object for main structure of report
     * @return baseBuilder
     */
    public DynamicReportBuilder createBuilder(){
        this.baseBuilder.setResourceBundle('grails-app/i18n/dj-messages')
        this.baseBuilder.setReportLocale(reportParams.getLocale())
        this.baseBuilder.setPrintBackgroundOnOddRows(true)
        this.baseBuilder.setUseFullPageWidth(true)
        this.baseBuilder.setDefaultStyles(this.footerFontStyle, this.footerFontStyle, this.footerFontStyle, this.footerFontStyle)
        this.buildFrame()
        return this.baseBuilder

    }

    /**
     * Procedure which creates Frame for report, creates header and footer, fills it with author, report title,
     * report type, date of creation and paging
     */
    public void buildFrame(){
        AutoText title = new AutoText(reportParams.getTitle(), AutoText.POSITION_HEADER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_LEFT), 200)
        title.setStyle(this.headerFontStyle)
        AutoText createdBy = new AutoText(reportParams.getMessage("report.header.made_by") + ": " + (reportParams.getAuthor()?: "") + ", " + dateTimeFormatter.format(reportParams.getCreationDate())?:"", AutoText.POSITION_HEADER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_RIGHT), 300)
        createdBy.setStyle(this.headerFontStyle)
        AutoText chartType = new AutoText(reportParams.getNamedChart(), AutoText.POSITION_FOOTER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_LEFT), 200)
        //chartType.setStyle(this.footerFontStyle)
        AutoText paging = new AutoText(AutoText.AUTOTEXT_PAGE_X_OF_Y, AutoText.POSITION_FOOTER, HorizontalBandAlignment.buildAligment(AutoText.ALIGMENT_RIGHT))
        //paging.setStyle(this.footerFontStyle)
        //TODO:Znaleźć przyczynę dla której nadanie w stopce stylu powoduje znikanie elementów
        baseBuilder.addAutoText(title)
        baseBuilder.addAutoText(createdBy)
        this.baseBuilder.addAutoText(chartType)
        this.baseBuilder.addAutoText(paging)
        this.baseBuilder.setTitleStyle(titleStyle)
        this.baseBuilder.setTitle(reportParams.getTitle())
        this.buildFrontpage()
    }
    /**
     * Procedure which builds frontpage structure for report
     */
    public void buildFrontpage(){
        def frontpage = new DynamicReportBuilder()
        frontpage.setUseFullPageWidth(true)
        frontpage.setWhenNoDataAllSectionNoDetail()
        frontpage.addColumn(this.createLeftHeaderColumn())
        frontpage.addColumn(this.createRightHeaderColumn())
        this.baseBuilder.addConcatenatedReport(frontpage.build(), new ClassicLayoutManager(), "header", DJConstants.DATA_SOURCE_ORIGIN_PARAMETER, DJConstants.DATA_SOURCE_TYPE_COLLECTION, false)
        this.buildSpecyficReport()
    }
    /**
     * Generate left header content as list of strings
     * @return leftHeaderContent
     */
    abstract public List getLeftHeaderData()
    /**
     * Generate report left header title as String
     * @return leftHeaderTitle
     */
    abstract public String getLeftHeaderTitle()
    /**
     * Generate right header content as list of string values
     * @return rightHeaderContent
     */
    abstract public List getRightHeaderData()
    /**
     * Generate report right header title as String
     * @return rightHeaderTitle
     */
    abstract public String getRightHeaderTitle()
    /**
     * If Report needs some custom styles this method initialize them
     */
    abstract public void buildSpecyficStyles()
    /**
     * This method create all custom layout structures for concrete report
     */
    abstract public void buildSpecyficReport()

}
