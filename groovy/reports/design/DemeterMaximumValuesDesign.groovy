package anteeo.demeter.reports.design

import ar.com.fdvs.dj.core.DJConstants
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.CustomExpression
import ar.com.fdvs.dj.domain.builders.ColumnBuilder
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn
import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors

import java.text.SimpleDateFormat

/**
 * Created by Piotr Czubek on 3/20/16.
 */
@CompileStatic
@InheritConstructors
class DemeterMaximumValuesDesign extends DemeterReportFrameDesign{

    public void buildSpecyficStyles(){
    }

    public List getRightHeaderData(){
        def formattedMpsList = []
        List<Map<String,Object>> mps = this.reportParams.getMpsData()
        for(mp in mps){
            formattedMpsList.push("${mp.name} [${mp.unit}]")
        }

        return formattedMpsList
    }
    public String getLeftHeaderTitle(){
        return this.reportParams.getMessage("report.maximalValues.leftHeader")
    }
    public List getLeftHeaderData() {
        return ["od ${this.dateTimeFormatter.format(reportParams.getStartDate().getTime())} do ${this.dateTimeFormatter.format(this.reportParams.getEndDate().getTime())}"]
    }
    public String getRightHeaderTitle(){
        return this.reportParams.getMessage("report.maximalValues.rightHeader")
    }
    private AbstractColumn createTimelineColumn(Map mp){
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.date')}")
        col.setStyle(this.timelineContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty((String)"timeline${mp.id}", Long.class.getName())
        col.setCustomExpression(timelineCustomExpression(dateTimeFormatter, (String)"timeline${mp.id}"))
        return col.build()
    }
    private AbstractColumn createRowNumberColumn(){
        def col = ColumnBuilder.getNew()
        col.setTitle("L.p.")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setWidth(10)
        col.setCustomExpression(
                new CustomExpression() {
                    public Object evaluate(Map fields, Map variables, Map parameters) {
                        Integer count = (Integer) variables.get("REPORT_COUNT")
                        return count
                    }
                    public String getClassName() {
                        return String.class.getName();
                    }
                }
        )
        return col.build()
    }
    private AbstractColumn createValueColumn(Map mp){
        String fieldName = "value${mp.id}"
        def col = ColumnBuilder.getNew()
        col.setTitle("${mp.name} [${mp.unit}]")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty(fieldName, BigDecimal.class.getName())
        col.setPattern("###,##0.00")
        return col.build()
    }

    private CustomExpression timelineCustomExpression(SimpleDateFormat formatter, String fieldname){
        return new CustomExpression() {
            public Object evaluate(Map fields, Map variables, Map parameters) {
                Date date = new Date((Long)fields.get(fieldname))
                return formatter.format(date)
            }
            public String getClassName() {
                return String.class.getName();
            }
        }
    }
    public void buildSpecyficReport(){
        def table = new DynamicReportBuilder()
        table.setUseFullPageWidth(true)
        table.setWhenNoDataAllSectionNoDetail()
        table.addColumn(this.createRowNumberColumn())
        table.setDefaultStyles(this.tableHeaderStyle, this.tableHeaderStyle, this.tableHeaderStyle, this.tableContentStyle)
        table.setOddRowBackgroundStyle(this.oddRowsStyle)
        table.setPrintBackgroundOnOddRows(true)
        List<Map<String,Object>> mps = reportParams.getMpsData()
        for(mp in mps) {
            table.addColumn(this.createTimelineColumn(mp))
            table.addColumn(this.createValueColumn(mp))
        }
        this.baseBuilder.addConcatenatedReport(table.build(), new ClassicLayoutManager(), "tableData", DJConstants.DATA_SOURCE_ORIGIN_PARAMETER, DJConstants.DATA_SOURCE_TYPE_COLLECTION, false)
    }
}
