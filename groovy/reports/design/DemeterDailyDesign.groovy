package anteeo.demeter.reports.design

import ar.com.fdvs.dj.core.DJConstants
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.CustomExpression
import ar.com.fdvs.dj.domain.DJCalculation
import ar.com.fdvs.dj.domain.DynamicReport
import ar.com.fdvs.dj.domain.builders.ColumnBuilder
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import ar.com.fdvs.dj.domain.builders.SubReportBuilder
import ar.com.fdvs.dj.domain.entities.Parameter
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn
import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors

/**
 * Created by Piotr Czubek on 4/11/16.
 */


@CompileStatic
@InheritConstructors
/**
 * Class to create Report Layout for Daily Report
 */
class DemeterDailyDesign extends DemeterReportFrameDesign{

    public void buildSpecyficStyles(){
    }

    public List getRightHeaderData(){
        //TODO: Tu powinna zostać stworzona legenda po rozwiązaniu problemu z kolorowaniem taryf
        def formattedMpsList = []
        return formattedMpsList
    }
    public String getLeftHeaderTitle(){
        return this.reportParams.getMessage("report.daily.leftHeader")
    }
    public List getLeftHeaderData() {
        return ["od ${this.dateTimeFormatter.format(reportParams.getStartDate().getTime())} do ${this.dateTimeFormatter.format(this.reportParams.getEndDate().getTime())}"]
    }
    public String getRightHeaderTitle(){
        return this.reportParams.getMessage("report.daily.rightHeader")
    }
    private AbstractColumn createNameColumn(){
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.daily.pointName')}")
        col.setStyle(this.timelineContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty((String)"name", String.class.getName())
        return col.build()
    }

    private AbstractColumn createUnitColumn(){
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.daily.unit')}")
        col.setStyle(this.timelineContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty((String)"unit", String.class.getName())
        return col.build()
    }

    private AbstractColumn createRowNumberColumn(){
        def col = ColumnBuilder.getNew()
        col.setTitle("L.p.")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setWidth(10)
        col.setCustomExpression(
                new CustomExpression() {
                    public Object evaluate(Map fields, Map variables, Map parameters) {
                        Integer count = (Integer) variables.get("REPORT_COUNT")
                        return count
                    }
                    public String getClassName() {
                        return String.class.getName();
                    }
                }
        )
        return col.build()
    }
    private AbstractColumn createValueColumn(){
        String fieldName = "value"
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.daily.consumption')}")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty(fieldName, BigDecimal.class.getName())
        col.setPattern("###,##0.00")

        return col.build()
    }
    private AbstractColumn createCo2Column(){
        String fieldName = "co2"
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.daily.co2')}")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty(fieldName, BigDecimal.class.getName())
        col.setPattern("###,##0.0000")

        return col.build()
    }
    private AbstractColumn createCostColumn(){
        String fieldName = "cost"
        def col = ColumnBuilder.getNew()
        col.setTitle("${this.reportParams.getMessage('report.daily.cost')}")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty(fieldName, BigDecimal.class.getName())
        col.setPattern("###,##0.00")

        return col.build()
    }

    public void buildSpecyficReport(){
        reportParams.groupsStructure.each { Map group ->
            String subreportTitle = group.name
            Map<Integer, List> media = (Map<Integer, List<Map>>)group.media
            media.each { Integer mediaId, List<Map> points ->
                def table = new DynamicReportBuilder()
                table.setTitle("${subreportTitle} : ${this.reportParams.getMessage(reportParams.mediaUtils.getMediaLocaleString(mediaId))}")
                table.setUseFullPageWidth(true)
                //table.setWhenNoDataAllSectionNoDetail()
                table.addColumn(this.createRowNumberColumn())
                table.setDefaultStyles(this.tableHeaderStyle, this.tableHeaderStyle, this.tableHeaderStyle, this.tableContentStyle)
                table.setOddRowBackgroundStyle(this.oddRowsStyle)
                table.setGrandTotalLegend("${this.reportParams.getMessage('report.consumption.sum')}")
                table.setGrandTotalLegendStyle(tableHeaderStyle)
                table.setPrintBackgroundOnOddRows(true)
                table.addColumn(this.createNameColumn())
                table.addColumn(this.createUnitColumn())
                AbstractColumn valueColumn = this.createValueColumn()
                table.addColumn(valueColumn)
                table.addGlobalFooterVariable(valueColumn,DJCalculation.SUM)
                AbstractColumn co2Column = this.createCo2Column()
                table.addColumn(co2Column)
                table.addGlobalFooterVariable(co2Column,DJCalculation.SUM)
                AbstractColumn costColumn = this.createCostColumn()
                table.addColumn(costColumn)
                table.addGlobalFooterVariable(costColumn,DJCalculation.SUM)
                this.baseBuilder.addConcatenatedReport(table.build(), new ClassicLayoutManager(),(String)"tableData${group.id}${reportParams.mediaUtils.getMediaName(mediaId)}", DJConstants.DATA_SOURCE_ORIGIN_PARAMETER, DJConstants.DATA_SOURCE_TYPE_COLLECTION, false)
            }
        }
  }
}
