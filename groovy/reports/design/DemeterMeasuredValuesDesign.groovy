package anteeo.demeter.reports.design

import ar.com.fdvs.dj.core.DJConstants
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager
import ar.com.fdvs.dj.domain.CustomExpression
import ar.com.fdvs.dj.domain.Style
import ar.com.fdvs.dj.domain.builders.ColumnBuilder
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn
import ar.com.fdvs.dj.domain.entities.conditionalStyle.ConditionalStyle
import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.apache.commons.beanutils.BeanUtils

import java.awt.Color
import java.text.SimpleDateFormat


/**
 * Created by Piotr Czubek on 3/7/16.
 */
@CompileStatic
@InheritConstructors
public class DemeterMeasuredValuesDesign extends DemeterReportFrameDesign {

    public void buildSpecyficStyles(){

    }

    public List getRightHeaderData(){
        def formattedMpsList = []
        List<Map<String,Object>> mps = reportParams.getMpsData()

        for(mp in mps){
            formattedMpsList.push("${mp.name} [${mp.unit}]")
        }

        return formattedMpsList
    }
    public String getLeftHeaderTitle(){
        return this.reportParams.getMessage("report.measuredValues.leftHeader")
    }
    public List getLeftHeaderData() {
        return ["od ${this.dateTimeFormatter.format(reportParams.getStartDate().getTime())} do ${this.dateTimeFormatter.format(reportParams.getEndDate().getTime())}"]
    }
    public String getRightHeaderTitle(){
        return reportParams.getMessage("report.measuredValues.rightHeader")
    }
    private AbstractColumn createTimelineColumn(Map mp){
        def col = ColumnBuilder.getNew()
        String columnIdentifier = "timeline${mp.id}"
        col.setTitle("${reportParams.getMessage('report.date')}")
        col.setStyle(this.timelineContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setColumnProperty(columnIdentifier, Long.class.getName())
        col.setCustomExpression(timelineCustomExpression(dateTimeFormatter, columnIdentifier))
        return col.build()
    }
    private AbstractColumn createRowNumberColumn(){
        def col = ColumnBuilder.getNew()
        col.setTitle("L.p.")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.setWidth(10)
        col.setCustomExpression(
            new CustomExpression() {
                public Object evaluate(Map fields, Map variables, Map parameters) {
                    Integer count = (Integer) variables.get("REPORT_COUNT")
                    return count
                }
                public String getClassName() {
                    return String.class.getName();
                }
            }
        )
        return col.build()
    }
    private AbstractColumn createValueColumn(Map mp){
        String fieldName = "value${mp.id}"
        String conditionFieldName = "state${mp.id}"
        List conditionalStyles = createConditionalStyles(conditionFieldName)
        def col = ColumnBuilder.getNew()
        col.setTitle("${mp.name} [${mp.unit}]")
        col.setStyle(this.tableContentStyle)
        col.setHeaderStyle(this.tableHeaderStyle)
        col.addConditionalStyles(conditionalStyles)
        col.setColumnProperty(fieldName, BigDecimal.class.getName())
        col.setPattern("###,##0.00")
        return col.build()
    }


    private List createConditionalStyles(String fieldName){
        List conditionalStyles = []
        Style gapStyleDef = (Style)BeanUtils.cloneBean(this.tableContentStyle)
        gapStyleDef.setBackgroundColor(Color.RED)
        gapStyleDef.setTransparent(false)

        def valueGapCondition = new GapCondition(fieldName, true)
        def valueNormalCondition = new GapCondition(fieldName, false)
        ConditionalStyle gapStyle = new ConditionalStyle(valueGapCondition, gapStyleDef)
        ConditionalStyle normalStyle = new ConditionalStyle(valueNormalCondition, this.tableContentStyle)
        conditionalStyles.add(gapStyle)
        conditionalStyles.add(normalStyle)
        return conditionalStyles
    }

    private CustomExpression timelineCustomExpression(SimpleDateFormat formatter, String columnIdentifier){
        return new CustomExpression() {
            public Object evaluate(Map fields, Map variables, Map parameters) {
                Date date = new Date((Long)fields.get(columnIdentifier))
                return formatter.format(date)
            }
            public String getClassName() {
                return String.class.getName();
            }
        }
    }
    public void buildSpecyficReport(){
        def table = new DynamicReportBuilder()
        List<Map<String,Object>> mps = reportParams.getMpsData()
        table.setUseFullPageWidth(true)
        table.setWhenNoDataAllSectionNoDetail()
        table.addColumn(this.createRowNumberColumn())
        table.addColumn(this.createTimelineColumn(mps[0]))
        table.setDefaultStyles(this.tableHeaderStyle, this.tableHeaderStyle, this.tableHeaderStyle, this.tableContentStyle)
        table.setOddRowBackgroundStyle(this.oddRowsStyle)
        table.setPrintBackgroundOnOddRows(true)
        for(mp in mps) {
            table.addColumn(this.createValueColumn(mp))
            table.addField("state${mp.id}", Integer.class.getName())
        }
        this.baseBuilder.addConcatenatedReport(table.build(), new ClassicLayoutManager(), "tableData", DJConstants.DATA_SOURCE_ORIGIN_PARAMETER, DJConstants.DATA_SOURCE_TYPE_COLLECTION, false)
    }
}
