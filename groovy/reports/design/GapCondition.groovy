package anteeo.demeter.reports.design

import ar.com.fdvs.dj.domain.entities.conditionalStyle.ConditionStyleExpression
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 3/18/16.
 */
import java.util.Map
import ar.com.fdvs.dj.domain.CustomExpression
@CompileStatic
class GapCondition extends ConditionStyleExpression implements CustomExpression {
    private String statusFieldName
    private Boolean forGap


    public GapCondition(String statusFieldName, Boolean forGap) {
        this.statusFieldName = statusFieldName
        this.forGap = forGap
    }
    public Object evaluate(Map fields, Map variables, Map parameters) {
        Object value = getCurrentValue()
        fields.get(statusFieldName)
        if(value == null && ((Integer)fields[statusFieldName]) < 0){
            if(forGap) {
                return Boolean.valueOf(true)
            }
            else{
                return Boolean.valueOf(false)
            }
        }
        else {
            if (forGap) {
                return Boolean.valueOf(false)
            } else {
                return Boolean.valueOf(true)
            }
        }
    }
    public String getClassName() {
        return Boolean.class.getName();
    }
}
