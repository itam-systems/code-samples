package anteeo.demeter.reports.data

import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic


/**
 * Created by Piotr Czubek on 4/3/16.
 */
@CompileStatic
class NMaxValues implements IReducedValuesManipulator {
    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>
    private List<Integer> valuesCounts

    public NMaxValues(List<Integer> valuesCounts){
        this.valuesCounts = valuesCounts
    }
    public void calculate(List<MeteringPointValuesInRange> mpValues){
        List<MeteringPointValuesInRange> tmpDataRange = [] as List<MeteringPointValuesInRange>
        mpValues.each{
            tmpDataRange.push(it.clone())
        }
        for(Integer pointIndex = 0; pointIndex < tmpDataRange.size();pointIndex++){
            MeteringPointValuesInRange entry = tmpDataRange[pointIndex]
            if (valuesCounts[pointIndex] != null) {
                entry.sortByValue(MeteringPointValuesInRange.DESC)
                List<ReducedMeteringPointValue> data = entry.getData()
                Integer limit = 0
                if(data.size() < valuesCounts[pointIndex]){
                    limit = entry.data.size()
                }
                else{
                    limit = valuesCounts[pointIndex]
                }
                MeteringPointValuesInRange tmpVals = new MeteringPointValuesInRange(entry.point.nodeid, entry.start, entry.end, true, entry.valuesInterval)
                tmpVals.setCalculationAlgorithm(entry.calculationAlgorithm)
                tmpVals.setDataType(entry.dataType)
                for (int dataIndex = 0; dataIndex < limit; dataIndex++) {
                    tmpVals.push(data[dataIndex])
                }
                values.push(tmpVals)
            } else {
                values.push(entry)
            }
        }
    }
    public List<MeteringPointValuesInRange> getResult(){
        return values
    }
}
