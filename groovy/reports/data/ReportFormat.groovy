package anteeo.demeter.reports.data

import anteeo.demeter.MeteringPoint
import anteeo.demeter.Tariff
import anteeo.demeter.TariffZone
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.data.ReducedMeteringPointValue
import anteeo.demeter.energy.TariffService
import anteeo.demeter.media.MediaUtils
import anteeo.demeter.reports.core.ReportMpValue
import anteeo.demeter.reports.design.DemeterReportFrameDesign
import grails.util.Holders
import groovy.transform.CompileStatic
import org.apache.log4j.Logger
import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartUtilities
import org.jfree.chart.JFreeChart
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.xy.IntervalXYDataset
import org.jfree.data.xy.XYSeries
import org.jfree.data.xy.XYSeriesCollection
import sun.misc.BASE64Encoder

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.math.RoundingMode

/**
 * Created by Piotr Czubek on 4/4/16.
 */
@CompileStatic
public class ReportFormat {
    Locale locale
    private List<MeteringPointValuesInRange> days
    private List<MeteringPointValuesInRange> hours
    private List<MeteringPointValuesInRange> quarters
    private List<MeteringPointValuesInRange> maxValues
    private List<List> tariffs
    private List<List<Map>> zonesMarks
    private List<Map> zonesSums
    private Integer mediaId

    Logger log = Logger.getLogger(getClass())

    TariffService tariffService = (TariffService)Holders.grailsApplication.mainContext.getBean("tariffService")

    ReportFormat(Locale locale){
        this.locale = locale
    }

    public void prepareDaily( List<MeteringPointValuesInRange> days,
                              List<MeteringPointValuesInRange> hours,
                              List<MeteringPointValuesInRange> quarters,
                              List<MeteringPointValuesInRange> maxValues,
                              List<List> tariffs,
                              List<List<Map>> zonesMarks,
                              List<Map> zonesSums,
                              Integer mediaId){
        this.days = days
        this.hours = hours
        this.quarters = quarters
        this.maxValues = maxValues
        this.tariffs = tariffs
        this.zonesMarks = zonesMarks
        this.zonesSums = zonesSums
        this.mediaId = mediaId
    }

    public static List<Map> simpleFormatter(List<MeteringPointValuesInRange> points) {
        List<Map> ret = [] as List<Map>
        Integer maxSize = 0
        for (int i = 0; i < points.size(); i++) {
            if (points[i].size() > maxSize) {
                maxSize = points[i].size()
            }
        }
        for (int i = 0; i < maxSize; i++) {
            Map tmpMap = [:] as Map
            points.each { MeteringPointValuesInRange values ->
                tmpMap += ReportMpValue.build(values.data.getAt(i)).asMap()
            }
            ret.push(tmpMap)
        }
        return ret
    }

    public static List<Map> consumptionFormatter(List<MeteringPointValuesInRange> points, List<Map> groupInfo, List<Map> zonesConsumptionSummary) {
        List<Map> ret = [] as List<Map>
        Integer maxSize = 0
        for (int i = 0; i < points.size(); i++) {
            if (points[i].size() > maxSize) {
                maxSize = points[i].size()
            }
        }
        for (int i = 0; i < maxSize; i++) {
            for(Integer pointIndex = 0; pointIndex < points.size(); pointIndex++){
                MeteringPointValuesInRange values = points[pointIndex]
                Map tmpMap = [:] as Map
                String name = groupInfo[pointIndex]["name"]
                ReportMpValue val = ReportMpValue.build(values.data.getAt(i))
                tmpMap[(String) "name"] = name.split(":")[0]
                tmpMap[(String) "unit"] = groupInfo[pointIndex]["unit"]
                tmpMap[(String) "value"] = val.value
                tmpMap[(String) "co2"] = (val.value != null) ? val.value * values.point.co2Factor: null
                BigDecimal cost = 0
                if(zonesConsumptionSummary[pointIndex] != null){
                    Map<String,Map> zonesConsumption = zonesConsumptionSummary[pointIndex]
                    zonesConsumption.each{String key, Map value ->
                        cost = cost + (BigDecimal)value.cost
                    }
                }
                tmpMap[(String) "cost"] = cost

                ret.push(tmpMap)
            }

        }
        return ret
    }

    public List<Map> dailyFormat() {
        List<Map> ret = [] as List<Map>
        for(Integer dayIndex = 0; dayIndex<this.days.size(); dayIndex++){
            MeteringPointValuesInRange values = this.days[dayIndex]
            for (Integer dayValueIndex = 0; dayValueIndex < values.data.size(); dayValueIndex++) {
                ret.push(generateDayPageDatasource(
                                values.data[dayValueIndex],
                                values.point,
                                hours[dayIndex],
                                quarters[dayIndex],
                                tariffs[dayValueIndex],
                                maxValues[dayValueIndex],
                                zonesMarks[dayIndex],
                                zonesSums[dayIndex]
                        )
                )
            }
        }
        return ret
    }

    public Map generateDayPageDatasource(
            ReducedMeteringPointValue dayValue,
            MeteringPoint point,
            MeteringPointValuesInRange hours,
            MeteringPointValuesInRange quarters,
            List<Map> tariff,
            MeteringPointValuesInRange maxValue,
            List<Map> zonesMarks,
            Map zonesSums
    ) {
        MediaUtils mediaUtils = new MediaUtils()
        log.debug "max ${maxValue?.data?.size()}"
        ReducedMeteringPointValue max = maxValue?.data?.getAt(0)
//        println "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"
//        println "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"
//        println "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1"
//        println "PointName: ${point.name}"
//        println "Tariff: ${tariff}"
//        println "Zones sums: ${zonesSums}"


        Tariff tariffObject
        List<TariffZone> zones
        if(tariff != null){
            tariffObject = (Tariff)tariff[0]?.tariff
            zones = (List<TariffZone>)tariff[0]?.zonesObjects
        }
//        println "Zones: ${zones}"

        List strefy = [] as List
        zonesSums?.each{ key, value ->
            Map formatted = [:] as Map
//            println "key: ${Long.parseLong((String)key)}"
            TariffZone zoneObject = zones.find {it.id == Long.parseLong((String)key)}
//            println "ZoneObject: ${zoneObject}"
            formatted["symbol"] = zoneObject.symbol
            formatted["nazwa"] = zoneObject.description
            formatted["zuzycie"] = ((BigDecimal) ((Map) value).consumption).doubleValue()
            formatted["procent"] = countPercentForZone(((BigDecimal) ((Map) value).consumption), dayValue.val)
            strefy.push(formatted)
        }
        Map day = [:] as Map
        day[(String) "taryfa"] = tariffObject?.name
        day[(String) "rodzajEnergii"] = mediaUtils.getMediaLocaleStringResolved(mediaId, locale)
        day[(String) "jednostkaEnergii"] = mediaUtils.getConsumptionUnit(mediaId)
        day[(String) "pietRodzajEnergii"] = mediaUtils.getConsumptionInTimeUnit(mediaId)
        day[(String) "bezstrefowoEnergia"] = mediaUtils.getNoZoneConsumptionName(mediaId)
        day[(String) "energiaLacznie"] = dayValue.val?.doubleValue()
        day[(String) "licznik"] = point.name.split(":")[0]
        day[(String) "data"] = DemeterReportFrameDesign.dateFormatter.format(new Date(dayValue.time))
        day[(String) "mocUmowna"] = null //TODO: Do obsłużenia po merge'u z testingiem
        day[(String) "strefy"] = strefy
//        if(zonesSums != null){
//            //TODO: Struktura dla ZonesSum[["symbol": "A", "nazwa": "całodobowo", "zużycie": 22.toDouble(), "procent": 99.toDouble()]]
//        }
        day[(String) "nazwa"] = "To niby nazwa"
        day[(String) "wykres"] = generateDailyChart(hours, "${mediaUtils.getMediaLocaleStringResolved(mediaId, locale)} [${mediaUtils.getConsumptionUnit(mediaId)}]")
        day[(String) "mocMaksymalna"] = max?.val?.doubleValue()
        day[(String) "godziny"] = generateHoursDatasource(hours.getData(), quarters.getData(), max, zonesMarks)

        return day
    }

    public List generateHoursDatasource(
            List<ReducedMeteringPointValue> hours,
            List<ReducedMeteringPointValue> quarters,
            ReducedMeteringPointValue maxValue,
            List<Map> zonesMarks
    ) {

        List ret = []
        Integer quarterValueIndex = 0
        log.debug "Tablica kwadransów - ${quarters.size()}, a ostatni to: ${quarters.last()}, pierwszy to: ${quarters.first()}"
        24.times { Integer hourValueIndex ->
            Map hour = [:] as Map
            hour[(String) "godzina"] = (hourValueIndex % 24).toString()
            hour[(String) "energia"] = hours[hourValueIndex]?.val?.doubleValue()
            Map zone = zonesMarks[hourValueIndex]
            hour[(String) "strefaSymbol"] = (zone != null && zone.containsKey("zoneSymbol")) ? zone.zoneSymbol : null
            hour[(String) "strefaKolor"] = (zone != null && zone.containsKey("zoneColor")) ? zone.zoneColor : null
            4.times { Integer idx ->
                hour[(String) "kwadrans${idx + 1}"] = quarters[quarterValueIndex] != null ? quarters[quarterValueIndex].val?.doubleValue() : null
                if (quarters[quarterValueIndex]?.val == maxValue?.val) {
                    hour[(String) "kwadransMax"] = (Integer) (idx + 1)
                }
                quarterValueIndex += 1
            }
            ret.push(hour)
        }
        return ret
    }

    static String generateDailyChart(MeteringPointValuesInRange dataSet, String label) {
        IntervalXYDataset dataset
        final XYSeries series = new XYSeries("");
        int a = 0;
        dataSet.data.each { ReducedMeteringPointValue hour ->
            series.add(a, hour.val)
            a++
        }
        dataset = new XYSeriesCollection(series)
        File imageFile = new File("${DemeterReportFrameDesign.chartsPath}/dziennyWykres-${dataSet.point.id}-${dataSet.start.time}-${dataSet.end.time}.jpg")
        final JFreeChart chart = ChartFactory.createXYBarChart(null, "Godzina", false, label, dataset, PlotOrientation.VERTICAL, false, false, false)
        try {
            ChartUtilities.saveChartAsJPEG(imageFile, chart, 442, 264)
        } catch (IOException e) {
            e.printStackTrace()
        }
        BufferedImage img = ImageIO.read(imageFile)

        String encodedImage = encodeImageToString(img, "jpg")
        imageFile.delete()
        return encodedImage

    }

    public static String encodeImageToString(BufferedImage image, String type) {
        String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            byte[] imageBytes = bos.toByteArray();

            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);

            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
    public static BigDecimal countPercentForZone(BigDecimal zoneUsage, BigDecimal totalUsage){
        BigDecimal factor = zoneUsage/totalUsage
        BigDecimal percent = factor * 100
        return percent.setScale(1, RoundingMode.CEILING)
    }

}