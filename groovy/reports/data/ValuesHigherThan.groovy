package anteeo.demeter.reports.data

import anteeo.demeter.data.IReducedValuesManipulator
import anteeo.demeter.data.MeteringPointValuesInRange
import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic

/**
 * Created by Piotr Czubek on 4/3/16.
 */

@CompileStatic
class ValuesHigherThan implements IReducedValuesManipulator{
    private List <MeteringPointValuesInRange> values = [] as List<MeteringPointValuesInRange>
    private List<BigDecimal> valueLimits

    public ValuesHigherThan(List<BigDecimal> valueLimit){
        this.valueLimits = valueLimit
    }
    public void calculate(List<MeteringPointValuesInRange> mpValues){
        for(Integer pointIndex = 0; pointIndex < mpValues.size(); pointIndex++){
            MeteringPointValuesInRange entry = mpValues[pointIndex]
            if(valueLimits[pointIndex] != null) {
                entry.sortByValue(MeteringPointValuesInRange.DESC)
                List<ReducedMeteringPointValue> data = entry.getData()
                MeteringPointValuesInRange tmpVals = new MeteringPointValuesInRange(entry.point.nodeid, entry.start, entry.end, true, entry.valuesInterval)
                for (int j = 0; j < data.size(); j++) {
                    if (data[j].val > valueLimits[pointIndex]) {
                        tmpVals.push(data[j])
                    }
                }
                values.push(tmpVals)
            }
            else{
                values.push(entry)
            }
        }
    }
    public List<MeteringPointValuesInRange> getResult(){
        return values
    }
}
