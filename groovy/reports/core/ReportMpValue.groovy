package anteeo.demeter.reports.core

import anteeo.demeter.data.ReducedMeteringPointValue
import groovy.transform.CompileStatic
import groovy.transform.ToString

/**
 * Created by Piotr Czubek on 4/1/16.
 */
@ToString(includeNames = true)
@CompileStatic
class ReportMpValue implements Serializable {
    Long mpId
    Long timeline
    BigDecimal value
    Integer state

    public static ReportMpValue build(Long mpId, Long time, BigDecimal value, Integer state = 0){
        return new ReportMpValue(
                mpId: mpId,
                timeline: time,
                value: value,
                state: state
        )
    }

    public static ReportMpValue build(ReducedMeteringPointValue baseVal) {
        if(baseVal == null){
            return new ReportMpValue()
        }
        return new ReportMpValue(
                mpId: baseVal.pointId,
                timeline: baseVal.time,
                value: baseVal.val,
                state: baseVal.status
        )
    }

    public Map asMap(){
        Map ret = [:] as Map
        ret[(String)"timeline${mpId}"] = timeline
        ret[(String)"value${mpId}"] = value
        ret[(String)"state${mpId}"] = state
        return ret
    }
}

