package anteeo.demeter.reports.core

import anteeo.demeter.Agreement
import anteeo.demeter.AgreementService
import anteeo.demeter.AgreementZonePriceList
import anteeo.demeter.Device
import anteeo.demeter.UserService
import anteeo.demeter.media.MediaUtils
import anteeo.demeter.reports.helpers.common.DeviceService
import grails.plugin.springsecurity.SpringSecurityService
import grails.util.Holders
import groovy.transform.CompileStatic
import org.apache.log4j.Logger
import org.joda.time.DateTime
import anteeo.demeter.NodeService

/**
 * Created by Piotr Czubek on 3/31/16.
 */
@CompileStatic
class ReportParams {

    private int typeId
    private Date creationDate
    private Date startDate
    private Date endDate
    private String title
    private String author
    private Locale locale
    private List mpsData
    private List<Map> groupsStructure

    NodeService nodeService = (NodeService)Holders.grailsApplication.mainContext.getBean("nodeService")
    DeviceService deviceService = (DeviceService)Holders.grailsApplication.mainContext.getBean("deviceService")
    SpringSecurityService springSecurityService = (SpringSecurityService)Holders.grailsApplication.mainContext.getBean("springSecurityService")
    AgreementService agreementService = (AgreementService)Holders.grailsApplication.mainContext.getBean("agreementService")
    UserService userService = (UserService)Holders.grailsApplication.mainContext.getBean("userService")


    Logger log = Logger.getLogger(getClass())


    public MediaUtils mediaUtils = new MediaUtils()
    public ReportParams(Map params){
        if(params.containsKey("type")) {
            if(params.type instanceof Integer) {
                this.setType((Integer)params.type)
            }else{
                try{
                    this.setType(Integer.getInteger(params.type.toString()))
                }
                catch(Exception e){
                    log.error e.message
                }
            }
        }

        if(params.containsKey("fromDate")){
            if(params.fromDate instanceof Date) {
                this.setStartDate((Date)params.fromDate)
            }
            else{
                try{
                    this.setStartDate(new Date( ((long)params.fromDate)))
                }
                catch(Exception e){
                    log.error e.message
                }
            }
        }
        if(params.containsKey("toDate")){
            if(params.toDate instanceof Date) {
                this.setEndDate((Date)params.toDate)
            }
            else{
                try{
                    this.setEndDate(new Date( ((long)params.toDate)))
                }
                catch(Exception e){
                    log.error e.message
                }
            }
        }
        if(params.containsKey("name")){
            this.setTitle((String)params.name)
        }
        if(params.containsKey("author")){
            this.setAuthor(params.author)
        } else {
            this.setAuthor(userService.getReadableUserName(springSecurityService.getCurrentUser()))

        }
        if(params.containsKey("creationDate")){
            if(params.creationDate instanceof Date) {
                this.setCreationDate((Date)params.creationDate)
            }
            else{
                try{
                    this.setCreationDate(new Date( ((long)params.endDate) * 1000 ))
                }
                catch(Exception e){
                    log.error e.message
                }
            }
        }
        else{
            this.setCreationDate(new Date())
        }
        if(params.containsKey("mpsData")){
            this.setMpsData((List)params.mpsData)
        }
        if(params.containsKey("tree")){
            List<Map> tree = (List<Map>)params.tree
            if(!tree.isEmpty()){
                groupsStructure = nodeService.buildNodesStructureForReport(tree)
                groupPointsByMedia()
            }
        }
    }

    public String getNamedChart(Integer typeId){
        String result = "Unknown report type, add it to DemeterReportFrameDesign"
        switch (typeId) {
            case 1:
                result = this.getMessage("report.type.measuredValues")
                break
            case 2:
                result = this.getMessage("report.type.averValues")
                break
            case 3:
                result = this.getMessage("report.type.maximumValues")
                break
            case 4:
                result = this.getMessage("report.type.changedValues")
                break
            case 5:
                result = this.getMessage("report.type.consumedEnergy")
                break
            case 6:
                result = this.getMessage("report.type.forecast_energy")
                break
            case 7:
                result = this.getMessage("report.type.changes_effects")
                break
            case 8:
                result = this.getMessage("report.type.light_change")
                break
            case 9:
                result = this.getMessage("report.type.repair_route")
                break
            case 10:
                result = this.getMessage("report.type.repair_prevention")
                break
            case 11:
                result = this.getMessage("report.type.daily")
                break
        }
        return result
    }

    public String getMessage(String message){
        return Holders.applicationContext.getMessage(message, null,message, this.locale)
    }
    public Date getStartDate(){
        return this.startDate
    }
    public void setStartDate(Date startDate){
        this.startDate = startDate
    }
    public setStartDate(DateTime startDate){
        this.startDate = startDate.toDate()
    }
    public Date getEndDate(){
        return this.endDate
    }
    public void setEndDate(Date endDate){
        this.endDate = endDate
    }
    public setEndDate(DateTime startDate){
        this.startDate = startDate.toDate()
    }
    public setType(Integer typeId){
        this.typeId = typeId
    }
    public Integer getType(){
        return this.typeId
    }
    public setAuthor(String author){
        this.author = author
    }
    public setAuthor(Object user){
        this.author = user.toString()
    }
    public String getAuthor(){
        return this.author
    }
    public setCreationDate(Date creationDate){
        this.creationDate = creationDate
    }
    public Date getCreationDate(){
        return this.creationDate
    }
    public setTitle(String title){
        this.title = title
    }
    public String getTitle(){
        return this.title
    }
    public void setMpsData(List mpsData){
        this.mpsData = mpsData
    }
    public List getMpsData(){
        return this.mpsData
    }

    public String getNamedChart(){
        return this.getNamedChart(this.typeId)
    }

    public void setLocale(Locale locale){
        this.locale = locale
    }

    public Locale getLocale(){
        return this.locale
    }

    public List<Map> getGroupsStructure(){
        return this.groupsStructure
    }

    public void groupPointsByMedia(){
        for(Integer groupIndex = 0; groupIndex < groupsStructure.size(); groupIndex++){
            Map group = groupsStructure[groupIndex]
            List<Map> points = (List<Map>)group.points
            Map<Integer,List> media = [:] as Map<Integer, List>
            points.each{ point ->
                Integer medium = mediaUtils.recogniseMediaForMp((String)point.nodeid)
                Device device = deviceService.findDeviceForPointNodeid((String)point.nodeid)
                point["device"] = device.id

                if(media.containsKey(medium)){
                    media[medium].push(point)
                }
                else{
                    media[medium] = [point]
                }
            }
            groupsStructure[groupIndex]["media"] = media

        }

    }



}
