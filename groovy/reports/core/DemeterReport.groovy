package anteeo.demeter.reports.core

import anteeo.demeter.MeteringPointService
import anteeo.demeter.Report
import anteeo.demeter.reports.helpers.common.MpValuesService
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder
import com.itextpdf.text.pdf.codec.Base64
import grails.util.Holders
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import net.sf.jasperreports.engine.JRAbstractExporter
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.*
import net.sf.jasperreports.export.SimpleExporterInput
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration
import net.sf.jasperreports.export.SimpleHtmlExporterOutput
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput
import net.sf.jasperreports.export.SimplePdfExporterConfiguration
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration
import net.sf.jasperreports.export.SimpleWriterExporterOutput
import net.sf.jasperreports.export.SimpleXlsExporterConfiguration
import org.apache.log4j.Logger

import javax.servlet.http.HttpServletRequest
import java.text.DecimalFormat
import java.text.SimpleDateFormat

import static anteeo.demeter.ITags.MP_DIFF_TAG
import static anteeo.demeter.ITags.MP_COUNTER_TAG

@CompileStatic
abstract class DemeterReport {
    MpValuesService mpValuesService = (MpValuesService)Holders.grailsApplication.mainContext.getBean("mpValuesService")
	MeteringPointService meteringPointService = (MeteringPointService)Holders.grailsApplication.mainContext.getBean("meteringPointService")

	Map<String, String> images = new HashMap<>();

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	SimpleDateFormat datePrintFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm")
	SimpleDateFormat blankHoursPrintFormat = new SimpleDateFormat("yyyy-MM-dd")
	SimpleDateFormat hoursFormat = new SimpleDateFormat("HH:mm")
	DecimalFormat decimalPrintFormat = new DecimalFormat("0.00")

    ReportParams reportParams


	Logger log = Logger.getLogger(getClass())

	/**
	 * Old method needs to be cutted out, converts domain report object to map for json compatibility, it overrides
	 * only basic parameters, rest is interpreted by asMap method in custom report class
	 * @param report
	 * @return reportAsMap
     */
	public static Map baseMap(Report report) {
		Map result = [:]
		result.id = report.id
		result.type = report.type
        Map slupedReport = (Map)new JsonSlurper().parseText(report.data)
        result << slupedReport
        return result
	}

	/**
	 * Saves new Report object in database
	 * @param params
	 * @return reportAsMap
     */
	public Map createInDb(Map params) {
		def data = create(params)
		Report rep = new Report(data)
		rep.save(flush: true, failOnError: true)
		return asMap(rep)
	}

	/**
	 * Updates domain report object using incoming parameters map
	 * @param params
	 * @return reportAsMap
     */
	public Map updateInDb(Map params) {
		Map obj = create(params)
		Report rep = Report.get(params.id as long)
		rep.data = obj.data
		rep.save(flush: true)
		return asMap(rep)
	}

	/**
	 * Creates custom parameters map report map object using its own map constructor
	 * @param params
	 * @return reportAsMap
     */
	public Map createInMap(Map params) {
		create(params)
	}

	/**
	 * Creates jasper abstract exporter object, depending on format which we currently needs, return type is interface, so we can made many operations dont thinking
	 * about concrete exporter type, all specific consiguration for exporter type should be made on this method
	 * @param format
	 * @param request
	 * @param stream
     * @return abstractExported
     */
	private JRAbstractExporter getExporter(String format, HttpServletRequest request, ByteArrayOutputStream stream) {
		JRAbstractExporter exporter
		switch (format.toLowerCase()) {
			case "pdf":
				exporter = new JRPdfExporter()
                def configuration = new SimplePdfExporterConfiguration()
                def output = new SimpleOutputStreamExporterOutput(stream)
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(output)
				break
			case 'html':
				exporter = new HtmlExporter()
                def configuration = new SimpleHtmlExporterConfiguration()
                def output = new SimpleHtmlExporterOutput(stream)
				output.setImageHandler(new HtmlResourceHandler() {

					@Override
					public void handleResource(String id, byte[] data) {
						images.put(id, "data:image/jpg;base64," + Base64.encodeBytes(data));
					}

					@Override
					public String getResourcePath(String id) {
						return images.get(id);
					}
				});
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(output)
				break
			case 'csv':
                exporter = new JRCsvExporter()
                def configuration = new SimpleCsvExporterConfiguration()
                def output = new SimpleWriterExporterOutput(stream)
				exporter.setConfiguration(configuration)
                exporter.setExporterOutput(output)
				break
			case 'xls':
				exporter = new JRXlsExporter()
                def configuration = new SimpleXlsExporterConfiguration()
                def output = new SimpleOutputStreamExporterOutput(stream)
                exporter.setConfiguration(configuration)
                exporter.setExporterOutput(output)
                break
		}
		exporter
	}

	/**
	 * Creates stream for specified format report
	 * @param report report object as map
	 * @param format required format declaration, possible values: pdf, html, csv, xls
	 * @param request needed only for locale, to change in the future
     * @return byteArrayStream
     */
	ByteArrayOutputStream getStream(Map report, String format, HttpServletRequest request) {
		List<JasperPrint> jasperPrints = getJasperPrintList(report, request)
		try {
            def result = new ByteArrayOutputStream()
			def exporter = getExporter(format, request, result)
            def input = SimpleExporterInput.getInstance(jasperPrints)
			exporter.setExporterInput(input)
            exporter.exportReport()
			return result
		}catch(exc) {
			log.error exc
			exc.printStackTrace()
            return new ByteArrayOutputStream()
		}
	}

	Boolean pointIsDiff(String nodeid){
		return meteringPointService.containsRequiredTag([MP_DIFF_TAG.toString()], meteringPointService.findByNodeid(nodeid).getTags())
	}

	Boolean pointIsCounter(String nodeid){
		return meteringPointService.containsRequiredTag([MP_COUNTER_TAG.toString()], meteringPointService.findByNodeid(nodeid).getTags())
	}

	abstract Map asMap(Report report)
	abstract Map create(Map params)
	abstract DynamicReportBuilder buildReport(Map<String, Object> report, HttpServletRequest request)
	abstract List<JasperPrint> getJasperPrintList(Map report, HttpServletRequest request)
    abstract Map getParams()
    abstract JRDataSource getDataSource(Map<String, Object> report, Locale locale)
}
