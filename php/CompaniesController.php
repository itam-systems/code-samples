<?php

namespace backend\controllers;

use backend\component\BaseController;
use GusApi\LawReport;
use GusApi\PhysicalReport;
use Imagine\Image\ManipulatorInterface;
use Yii;
use backend\models\Company;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use GusApi\GusApi;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\imagine\Image;

class CompaniesController extends BaseController
{
    /**
     * @param Behavior[] $behaviors
     */
    /**
     * @inheritdoc
     */

    public $gus;
    public $sid;

    public function init(){
        $this->gus = new GusApi(
            'a71c725fa7d5447a915a',
            new \GusApi\Adapter\Soap\SoapAdapter(
                \GusApi\RegonConstantsInterface::BASE_WSDL_URL,
                //\GusApi\RegonConstantsInterface::BASE_WSDL_ADDRESS //<--- production server / serwer produkcyjny
                \GusApi\RegonConstantsInterface::BASE_WSDL_ADDRESS_TEST
            //w przypadku serwera testowego użyj: RegonConstantsInterface::BASE_WSDL_ADDRESS_TEST
            )
        );
        $this->sid = $this->gus->login();
        $captcha = $this->gus->getCaptcha($this->sid);
        Yii::$app->fs->put('tmp/captcha.jpeg', base64_decode($captcha));
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => [
                        'company',
                        'update',
                        'create',
                        'index',
                        'user-company',
                        'from-gus',
                        'check-nip',
                        'update-logo',
                        'update-law-base'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],

            ],
        ];
        return $behaviors;
    }

    public function actionCompany(){
        $params = Yii::$app->getRequest()->getBodyParams();
        $model = null;
        if($params == null){
            return $this->returnError('Nie udało się pobrać firmy - brak parametrów');
        }
        if(array_key_exists('id', $params)) {
            $model = Company::findOne($params['id']);
        }
        if($model == null){
            return $this->returnError('Podana firma nie istnieje');
        }
        return $this->returnSuccess($model);
    }

    public function actionUserCompany(){
        $user_id = Yii::$app->user->identity->getId();
        $model = null;
        if($user_id != null) {
            $model = Company::find()->where(['user_id' => $user_id])->one();
        }
        if($model == null){
            return $this->returnError('Podana firma nie istnieje');
        }
        return $this->returnSuccess($model);
    }

    public function actionUpdate(){
        $params = Yii::$app->getRequest()->getBodyParams();
        if($params == null){
            return $this->returnError('Nie udało się zaktualizować firmy - brak parametrów');
        }
        $user_id = null;
        if(array_key_exists('user_company', $params) && $params['user_company'] == true) {
            $user_id = Yii::$app->user->identity->getId();
        }
        $model = (array_key_exists('id', $params)) ? Company::findOne($params['id']) : null;
        if ($model == null){
            $model = new Company();
        }
        $model->setAttributes($params, false);
        $model->setAttribute('user_id', $user_id);
        if(!$model->validate()){
            return $this->returnError('Nie udało się zaktualizować firmy - błędne parametry \n' . implode("\n",$model->getFirstErrors()));
        }

        if ($model->save()){
            return $this->returnSuccess($model, 'Firma ' . $model->full_name . ' został zaktualizowany pomyślnie');
        }
        else{
            return $this->returnError('Nie udało się zaktualizować firmy - błęd zapisu \n' . implode("\n",$model->getFirstErrors()));
        }
    }

    public function actionCreate(){
        $params = Yii::$app->getRequest()->getBodyParams();
        if ($params == null){
            return $this->returnError('Nie udało się utworzyć firmy - pusty zestaw parametrów');
        }
        $model = new Company();
        $model->setAttributes($params, false);
        if(!$model->validate()){
            return $this->returnError('Nie udało się utworzyć firmy - błędne parametry \n' . implode("\n",$model->getFirstErrors()));
        }
        if ($model->save()){
            return $this->returnSuccess($model, 'Firma ' . $model->full_name . ' została utworzona pomyślnie');
        }
        else{
            return $this->returnError('Nie udało się utworzyć firmy - błęd zapisu \n' . implode("\n",$model->getFirstErrors()));
        }
    }

    public function actionIndex(){
        $models = Company::find()->all();
        if(empty($models)){
            return $this->returnError('Brak firm do wyświetlenia');
        }
        else{
            return $this->returnSuccess($models);
        }
    }
    public function actionFromGus(){
        try{
            $params = Yii::$app->getRequest()->getBodyParams();
            if($params == null){
                return $this->returnError('Nie udało się pobrać firmy - brak parametrów');
            }
            $checked = $this->gus->checkCaptcha($this->sid, $params['captcha']);
            if(!$checked){
                return $this->returnError('Niepoprawny tekst z obrazka, spróbuj ponownie');
            }
            $model = null;
            try {
                if (array_key_exists('nip', $params)) {
                    $model = $this->gus->getByNip($this->sid, $params['nip']);
                } else if (array_key_exists('krs', $params)) {
                    $model = $this->gus->getByKrs($this->sid, $params['krs']);
                } else if (array_key_exists('regon', $params)) {
                    $model = $this->gus->getByRegon($this->sid, $params['regon']);
                } else {
                    return $this->returnError('Nieobsługiwany identyfikator');
                }
            }
            catch(Exception $e){
                return $this->returnError('Nie odnaleziono podmiotu');
            }
            if($model == null){
                return $this->returnError('Nie odnaleziono podmiotu');
            }
            $company = $this->getDetailedReport($model);

            return $this->returnSuccess(
                [
                    'name' => (string)$company->getName(),
                    'full_name' => (string)$company->getFullName(),
                    'nip' => (string)$company->getNip(),
                    'regon' => (string)$company->getRegon(),
                    'krs' => (string)$company->getKrs(),
                    'mail' => (string)$company->getMail(),
                    'address' => (string)$company->getAddress(),
                    'postal' => (string)$company->getPostal(),
                    'commune' => (string)$company->getCommune(),
                    'city' => (string)$company->getCity(),
                    'country' => (string)$company->getCountry()
                ]
            );


        } catch (\GusApi\InvalidUserKeyException $e) {
            return $this->returnError('Nieprawidłowy identyfikator');
        }
    }

    private function getDetailedReport($model){
        $company = null;
        if($model->getType() == 'f'){
            $company = $this->gus->getFullReport(
                $this->sid,
                $model,
                \GusApi\ReportTypes::REPORT_ACTIVITY_PHYSIC_PERSON
            );
            $companyDetails = $this->getPhysicalDetails($model, $company);
            return new PhysicalReport($company, $companyDetails);

        }
        else{
            $company = $this->gus->getFullReport(
                $this->sid,
                $model,
                \GusApi\ReportTypes::REPORT_PUBLIC_LAW
            );
            return new LawReport($company);
        }
    }

    private function getPhysicalDetails($model, $company){
        $reportUrl = null;
        if($company->dane->fiz_dzialalnosciCeidg){
            $reportUrl = \GusApi\ReportTypes::REPORT_ACTIVITY_PHYSIC_CEGID;
        }
        else if($reportUrl == null && $company->dane->fiz_dzialalnosciRolniczych){
            $reportUrl = \GusApi\ReportTypes::REPORT_ACTIVITY_PHYSIC_AGRO;
        }
        else if($reportUrl == null && $company->dane->fiz_dzialalnosciPozostalych){
            $reportUrl = \GusApi\ReportTypes::REPORT_ACTIVITY_PHYSIC_OTHER_PUBLIC;
        }
        return $this->gus->getFullReport(
            $this->sid,
            $model,
            $reportUrl
        );
        
    }

    public function actionCheckNip(){
        $params = Yii::$app->getRequest()->getBodyParams();
        if(!array_key_exists('nip', $params)){
            return $this->returnError('Brak numeru NIP');
        }
        $user_id = Yii::$app->user->identity->getId();
        $my_comapny = Company::find()->where(['user_id' => $user_id])->one();
        $model = Company::find()->where(['nip' => $params['nip']])->one();
        if($model == null || ($model->nip == $my_comapny->nip)){
            return $this->returnSuccess(['free' => true]);
        }
        else{
            return $this->returnSuccess(['free' => false], 'Numer nip jest już użyty');
        }
    }

    public function actionUpdateLogo(){
        if ( !empty( $_FILES ) ) {
            try {
                $user_id = Yii::$app->user->identity->getId();
                $filePath = $_FILES['file']['tmp_name'];
                $originalName = $_FILES['file']['name'];
                $logoFile = fopen($filePath, 'r');
                $fileExtension = pathinfo($originalName, PATHINFO_EXTENSION);
                $originalPath = 'company/logo_' . $user_id . '_original.' . $fileExtension;
                $bigThumbPath = 'company/logo_' . $user_id . '_thumb_big.' . $fileExtension;
                $smallThumbPath = 'company/logo_' . $user_id . '_thumb_small.' . $fileExtension;
                Yii::$app->fs->put($originalPath, $logoFile);
                Yii::$app->fs->put($bigThumbPath, Image::thumbnail(Yii::$app->fs->path . DIRECTORY_SEPARATOR . $originalPath, 200, 200, ManipulatorInterface::THUMBNAIL_INSET));
                Yii::$app->fs->put($smallThumbPath, Image::thumbnail(Yii::$app->fs->path . DIRECTORY_SEPARATOR . $originalPath, 120, 120, ManipulatorInterface::THUMBNAIL_INSET));
                $my_comapny = Company::find()->where(['user_id' => $user_id])->one();
                $my_comapny->setAttribute('logo', $originalPath);
                if($my_comapny->save()) {
                    return $this->returnSuccess(['originalPath' => $originalPath, 'bigThumbnailPath' => $bigThumbPath, 'smallThumbnailPath' => $smallThumbPath]);
                }
                else{
                    return $this->returnError("Wystąpił problem podczas uploadu pliku: \n\r" . implode("\n",$my_comapny->getFirstErrors()));
                }
            }
            catch(Exception $e){
                return $this->returnError("Wystąpił problem podczas uploadu pliku: \n\r" . $e->getMessage());
            }
        }
        else{
            return $this->returnError("Brak pliku w zapytaniu, upewnij się czy prawidłowo je formułujesz");
        }
    }

    public function actionUpdateLawBase(){
        $params = Yii::$app->getRequest()->getBodyParams();
        if($params == null){
            return $this->returnError('Nie udało się zaktualizować firmy - brak parametrów');
        }
        $user_id = Yii::$app->user->identity->getId();
        if($user_id == null){
            return $this->returnError("Nie udało się określić aktualnie zalogowanego użytkownika");
        }
        if(!array_key_exists('law_base', $params)) {
            return $this->returnError('Nie udało się zaktualizować firmy - brak parametru');
        }
        $model = Company::find()->where(['user_id' => $user_id])->one();
        if ($model == null){
            return $this->returnError("Określ najpierw dane firmy zanim wypełnisz dane o podstawie zwolnienia");
        }
        $model->setAttribute('law_base', $params['law_base']);
        if(!$model->validate()){
            return $this->returnError('Nie udało się zaktualizować firmy - błędne parametry \n' . implode("\n",$model->getFirstErrors()));
        }

        if ($model->save()){
            return $this->returnSuccess($model, 'Firma ' . $model->full_name . ' został zaktualizowany pomyślnie');
        }
        else{
            return $this->returnError('Nie udało się zaktualizować firmy - błęd zapisu \n' . implode("\n",$model->getFirstErrors()));
        }
    }

}
