<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property string $name
 * @property string $full_name
 * @property string $nip
 * @property string $regon
 * @property string $krs
 * @property string $iban
 * @property string $mail
 * @property string $address
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'full_name', 'nip'], 'required'],
            [['address', 'law_base'], 'string'],
            [['name', 'full_name', 'nip', 'regon', 'krs', 'iban', 'mail'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'full_name' => 'Full Name',
            'nip' => 'Nip',
            'regon' => 'Regon',
            'krs' => 'Krs',
            'iban' => 'Iban',
            'mail' => 'Mail',
            'address' => 'Address',
            'law_base' => 'Law base'
        ];
    }

    public function getUser(){
        $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
