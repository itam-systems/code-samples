<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 01.08.16
 * Time: 07:27
 */

namespace backend\component;


use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Controller;
use yii\web\Response;

class BaseController extends Controller
{

    public function init()
    {
        parent::init();
    }
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];
        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ],
        ];

        return $behaviors;
    }

    public function returnSuccess($data = [], $message = ''){
        return [
            'status' => 'success',
            'message' => $message,
            'data' => $data
        ];
    }

    public function returnError($message = '', $data = []){
        return [
            'status' => 'error',
            'message' => $message,
            'data' => $data
        ];
    }

}