<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'aliases' => [
        '@GusApi' => '@vendor/gus-api/src/GusApi',
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'fs' => [
            'class' => 'creocoder\flysystem\LocalFilesystem',
            'path' => '@webroot/storage',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' =>
                array(
                    [
                        'pattern' => 'api/<controller:users>/<action:(login|update-user|update-user-personal|change-password|create|check-username|check-email|check-iban|add-iban|set-default-iban|update-invoice-format|update-user-mail|update-user-config)>',
                        'route' => '<controller>/<action>',
                        'verb' => 'POST'
                    ],
                    [
                        'pattern' => 'api/<controller:users>/<action:(user-data|personal-user-data|export-roles|index|ibans|invoice-format|user-mail|user-config)>',
                        'route' => '<controller>/<action>',
                        'verb' => 'GET'
                    ],
                    [
                        'pattern' => 'api/<controller:companies>/<action:(update|create|from-gus|check-nip|update-logo|update-law-base)>',
                        'route' => '<controller>/<action>',
                        'verb' => 'POST'
                    ],
                    [
                        'pattern' => 'api/<controller:companies>/<action:(index|company|user-company)>',
                        'route' => '<controller>/<action>',
                        'verb' => 'GET'
                    ]
                )
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ]
    ],

    'params' => $params,
];
